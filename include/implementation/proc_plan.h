//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_PROC_PLAN_H
#define S3DFT_PROC_PLAN_H

/// \file This file contains a function which returns a process-plan as a tensor-domain.

#include <array> // std::array


namespace s3dft
{

std::size_t get_proc_id(const std::size_t& p, const std::size_t& i1, const std::size_t& i2, const std::size_t& i3);

std::array<std::size_t, 3> get_proc_coords(const std::size_t& p, const std::size_t& proc_rank);

} // namespace s3dft
#endif
