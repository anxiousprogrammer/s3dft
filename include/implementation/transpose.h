//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_TRANSPOSE_VSLICES_DIR2_H
#define S3DFT_TRANSPOSE_VSLICES_DIR2_H

/// \file This file contains functions for transposing slices of cubic tensors.

#include "tensor.h"


namespace s3dft
{

template<class Treal_t>
void transpose13(const tensor<Treal_t>& tens, tensor<Treal_t>& tens_tr);

template<class Treal_t>
void transpose23(const tensor<Treal_t>& tens, tensor<Treal_t>& tens_tr, const std::vector<std::size_t>& work_indices);

} // namespace s3dft

#endif
