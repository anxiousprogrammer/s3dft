//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_MATRIX_H
#define S3DFT_MATRIX_H

/// \file This file contains a construct which allows for matrix representation.

#include "implementation/data_pack.h"

#include <stdexcept> // std::runtime_error, std::invalid_argument
#include <memory> // std::unique_ptr
#include <cstdlib> // std::free


namespace s3dft
{

template<class Treal_t>
class matrix
{
    //+//////////////////////////////////////////
    // Member
    //+//////////////////////////////////////////
    
    data_pack<Treal_t> data_;
    std::size_t n_;
    
    // cache
    std::size_t size_;
    
    
public:
    
    //+//////////////////////////////////////////
    // Lifecycle
    //+//////////////////////////////////////////
    
    matrix(const std::size_t& n);
    
    matrix(const matrix&) = default;
    
    matrix(matrix&& rhs);
    
    matrix& operator=(const matrix&) = default;
    
    matrix& operator=(matrix&& rhs);
    
    ~matrix() = default;
    
    
    //+//////////////////////////////////////////
    // Access
    //+//////////////////////////////////////////
    
    inline complex<Treal_t>* data()
    {
        return data_.data_;
    }
    
    inline const complex<Treal_t>* data() const
    {
        return const_cast<matrix*>(this)->data();
    }
    
    // remark: don't use this in important kernels, this is a slower access mode
    inline complex<Treal_t>& operator()(const std::size_t& i1, const std::size_t& i2)
    {
        #ifndef NDEBUG
        if (i1 >= n_ || i2 >= n_)
        {
            throw std::invalid_argument("matrix_operator()_index_out_of_range_error");
        }
        #endif
        return data_.data_[i1 * n_ + i2];
    }
    
    inline const complex<Treal_t>& operator()(const std::size_t& i1, const std::size_t& i2) const
    {
        return const_cast<matrix*>(this)->operator()(i1, i2);
    }
    
    inline std::size_t get_n() const
    {
        return n_;
    }
    
    inline std::size_t size() const
    {
        return size_;
    }
};

} // namespace s3dft
#endif
