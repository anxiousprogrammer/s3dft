//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_TENSOR_H
#define S3DFT_TENSOR_H

/// \file This file contains a construct which allows for tensor representation.

#include "implementation/data_pack.h"

#include <stdexcept> // std::runtime_error, std::invalid_argument
#include <memory> // std::unique_ptr
#include <vector> // std::vector


namespace s3dft
{

template<class Treal_t>
class tensor
{    
    //+//////////////////////////////////////////
    // Member
    //+//////////////////////////////////////////
    
    data_pack<Treal_t> data_;
    std::size_t n_;
    std::size_t n_sq_;
    std::size_t size_;
    
    
public:

    //+//////////////////////////////////////////
    // Lifecycle
    //+//////////////////////////////////////////
    
    tensor(const std::size_t& n);
    
    tensor(const tensor&) = default;
    
    tensor(tensor&& rhs);
    
    tensor& operator=(const tensor&) = default;
    
    tensor& operator=(tensor&& rhs);
    
    ~tensor() = default;
    
    //+//////////////////////////////////////////
    // Access
    //+//////////////////////////////////////////
    
    inline complex<Treal_t>* get_slice_data(const std::size_t& index)
    {
        #ifndef NDEBUG
        if (index >= n_)
        {
            throw std::invalid_argument("tensor_get_slice_data_index_error");
        }
        #endif
        return data_.data_ + index * n_sq_;
    }
    
    inline const complex<Treal_t>* get_slice_data(const std::size_t& index) const
    {
        return const_cast<tensor*>(this)->get_slice_data(index);
    }
    
    inline complex<Treal_t>* data() // note: use only for MPI comm. purposes
    {
        return data_.data_;
    }
    
    inline const complex<Treal_t>* data() const
    {
        return const_cast<tensor*>(this)->data();
    }
    
    inline complex<Treal_t>& operator()(const std::size_t& i1, const std::size_t& i2, const std::size_t& i3)
    {
        #ifndef NDEBUG
        if (i1 >= n_ || i2 >= n_ || i3 >= n_)
        {
            throw std::invalid_argument("tensor_operator_index_error");
        }
        #endif
        return data_.data_[i1 * n_sq_ + i2 * n_ + i3];
    }
    
    inline const complex<Treal_t>& operator()(const std::size_t& i1, const std::size_t& i2, const std::size_t& i3) const
    {
        return const_cast<tensor*>(this)->operator()(i1, i2, i3);
    }
    
    inline std::size_t get_n() const
    {
        return n_;
    }
    
    inline std::size_t get_slice_size() const
    {
        return n_sq_;
    }
    
    inline std::size_t size() const
    {
        return size_;
    }
};


//+////////////////////////////////////////////////////////////////////////////////////////////////
// Utility functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
void tensor_zero(tensor<Treal_t>& op, const std::vector<std::size_t>& work_indices);

template<class Treal_t>
void tensor_copy(const tensor<Treal_t>& op1, tensor<Treal_t>& op2, const std::vector<std::size_t>& work_indices);

} // namespace s3dft
#endif
