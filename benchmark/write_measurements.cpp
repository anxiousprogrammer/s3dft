//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation of 'write_measurements.h'

#include "write_measurements.h"

#include <fstream> // std::ofstream
#include <iostream> // std::cerr


namespace s3dft
{
namespace benchmarking
{

//+//////////////////////////////////////////////
// implementation
//+//////////////////////////////////////////////

void write_measurements(const std::vector<double>& measurements, const std::string& filename)
{
    // preconditions
    if (measurements.empty())
    {
        std::cerr << "No measurement data available." << std::endl;
        return;
    }
    
    // open file
    auto file = std::ofstream(filename, std::ios_base::trunc);
    if (!file.is_open())
    {
        std::cerr << "Could not open file to which the measurements were to be written." << std::endl;
        return;
    }
    
    // write to file
    auto counter = std::size_t{1};
    for (const auto& measurement : measurements)
    {
        file << counter++ << " " << measurement << std::endl;
    }
    
    // close file
    file.close();
    if (file.is_open())
    {
        std::cerr << "Could not close file to which the measurements were to be written." << std::endl;
        return;
    }
}

} // namespace benchmarking
} // namespace s3dft
