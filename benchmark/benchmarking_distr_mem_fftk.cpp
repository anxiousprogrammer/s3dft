//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program for the FFTW3 API function (maybe Intel or FFTW3 implementation).

#include "write_measurements.h"

#include "tixl_w_mpi.h"

//#include "blitz/array.h"
#include "fftk.h"

#include <omp.h>
#include <mpi.h>

#include <cstdlib> // std::malloc
#include <fstream> // std::ofstream
#include <iostream> // std::cout
#include <cmath> // std::cbrt
#include <algorithm> // std::minmax
#include <cstring> // std::strlen
#include <cctype> // std::isdigit

#include <unistd.h> // sysconf

using namespace s3dft;

namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t n_;
    const std::size_t p_;
    FFTK fftk_ = {};
    Array<Real, 3> input_data_ = {};
    Array<Complex, 3> output_data_ = {};
    bool use_clflush_ = true;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& n, const std::size_t& p)
        : n_(n), p_(p)
    {
        if (!__builtin_cpu_supports("sse2"))
        {
            std::cerr << "Warning! The CPU does not have the \'sse2\' flag, a feature of which is used to forcefully evict "
                "cache lines to memory." << std::endl;
            use_clflush_ = false;
        }
        
        // init the FFTK object
        fftk_.Init("FFF", n_, n_, n_, p_);
    }
    
    ~exp_fn()
    {
        // clean up
        fftk_.Finalize();
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew
        input_data_.resize(fftk_.Get_RA_shape());
        output_data_.resize(fftk_.Get_FA_shape());
        
        // first touch
        constexpr auto cache_line_size = 64;
        const auto page_size = sysconf(_SC_PAGESIZE);
        const auto page_size_real = page_size / sizeof(Real);
        constexpr auto cache_line_size_real = cache_line_size / sizeof(Real);
        auto* input_data_raw = input_data_.data();
        for (auto ii = std::size_t{}; ii < input_data_.size(); ii += page_size_real)
        {
            input_data_raw[ii] = 0.0;
            if (use_clflush_)
            {
                for (auto jj = std::size_t{}; jj < page_size_real; jj += cache_line_size_real)
                {
                    _mm_clflush(input_data_raw + ii + jj);
                }
            }
        }
        const auto page_size_complex = page_size / sizeof(Complex);
        constexpr auto cache_line_size_complex = cache_line_size / sizeof(Complex);
        auto* output_data_raw = output_data_.data();
        for (auto ii = std::size_t{}; ii < output_data_.size(); ii += page_size_complex)
        {
            output_data_raw[ii] = 0.0;
            if (use_clflush_)
            {
                for (auto jj = std::size_t{}; jj < page_size_complex; jj += cache_line_size_complex)
                {
                    _mm_clflush(output_data_raw + ii + jj);
                }
            }
        }
    }
    
    void perform_experiment() final
    {
        fftk_.Forward_transform("FFF", input_data_, output_data_);
    }
    
    void finish() final
    {
    }
};

} // namespace impl

int main(int argc, char** argv)
{
    //+/////////////////
    // MPI init
    //+/////////////////
    
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // rank
    auto world_rank = int{};
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    
    // size
    auto world_size = int{};
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    //+/////////////////
    // extract the problem size
    // and filename of the measurements
    // log
    //+/////////////////
    
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument."
            << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    
    //+/////////////////
    // extract 'p'
    //+/////////////////
    
    if (argc < 3)
    {
        std::cerr << "Please provide the value of p as command line argument." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto p = std::stoul(argv[2]);
    if (p == 0 || p > world_size)
    {
        std::cerr << "Fatal error: invalid value of p provided." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    //+/////////////////
    // FFTK
    //+/////////////////
    
    // report
    if (world_rank == 0)
    {
        std::cout << "Starting runs for FFTK with N=" << n << ", p=" << p << " (N_task=" << world_size << ").\n";
        std::cout << "Thread count: " << omp_get_max_threads() << ".\n";
    }
    
    // perform the experiments
    const auto experiment_count = std::size_t{100}; // REMARK: used to be 250 but was cut down due to computational costs on cluster!
    auto ef = impl::exp_fn(n, p);
    const auto measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, ef, experiment_count);
    const auto result = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements);
    if (world_rank == 0)
    {
        tixl::output_results("Results of the runs with FFTK", result, 3 * 8 * n * n * n * n);
        benchmarking::write_measurements(measurements, std::string("fftk_measurements_p") + std::to_string(p)
            + std::string(".txt"));
    }
    
    // finalize MPI
    MPI_Finalize();
    
    return 0;
}
