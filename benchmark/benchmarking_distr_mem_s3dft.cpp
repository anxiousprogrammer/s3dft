//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of the distributed memory parallel 3D-DFT implementation.

#include "implementation/schedule_thread_work.h"
#include "distr_mem_3d_dft.h"

#include "write_measurements.h"
#include "set_first_touch.h"

#include "tixl_w_mpi.h"

#include "fftw/fftw3-mpi.h"

#include <omp.h>
#include <mpi.h>

#include <fstream> // std::ofstream
#include <iostream> // std::cout
#include <cmath> // std::cbrt
#include <cstring> // std::strlen
#include <cctype> // std::isdigit


using namespace s3dft;

namespace impl
{

class s3dft_exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t n_;
    const std::size_t b_;
    tensor<double>* data_ = nullptr;
    matrix<double>* coeff_block_ = nullptr;
    scratch_cache<double>* scache_ = nullptr;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    s3dft_exp_fn(const std::size_t& n)
        : n_(n), b_(compute_block_size(n_, MPI_COMM_WORLD)) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        data_ = new tensor<double>(b_);
        coeff_block_ = new matrix<double>(std::move(create_coeff_block_matrix<double>(n_, b_, MPI_COMM_WORLD)));
        scache_ = new scratch_cache<double>(std::move(create_scratch_cache<double>(b_)));
        if (data_ == nullptr || coeff_block_ == nullptr || scache_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        }
        
        // first-touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(data_->get_n(), 1);
            benchmarking::set_first_touch(*data_, work_indices);
        }
    }
    
    void perform_experiment() final
    {
        execute(n_, *data_, *coeff_block_, *scache_, MPI_COMM_WORLD);
    }
    
    void finish() final
    {
        delete data_;
        delete coeff_block_;
        delete scache_;
    }
};

} // namespace impl

int main(int argc, char** argv)
{
    //+/////////////////
    // MPI init
    //+/////////////////
    
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // rank
    auto world_rank = int{};
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    
    // calculate the problem size
    auto world_size = int{};
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    const auto p_as_fp = std::cbrt(static_cast<double>(world_size));
    const auto p = static_cast<std::size_t>(p_as_fp);
    if (std::fabs(static_cast<double>(p) - p_as_fp) > 1E-15)
    {
        std::cerr << "Fatal error: the number of processes must be cubic." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    //+/////////////////
    // extract the problem size
    // and filename of the measurements
    // log
    //+/////////////////
    
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument."
            << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    const auto b = compute_block_size(n, MPI_COMM_WORLD);
    if (n < 10 || b > 5000)
    {
        std::cerr << "Fatal error: invalid problem size provided. The program will now be aborted." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    //+/////////////////
    // S3DFT
    //+/////////////////
    
    // report
    if (world_rank == 0)
    {
        std::cout << "Starting runs for S3DFT with N=" << n << ", p=" << p << " (N_task=" << world_size << ").\n"
            << "Thread count: " << omp_get_max_threads() << ".\n";
    }
    
    // perform the experiments
    const auto experiment_count = std::size_t{100}; // REMARK: used to be 250 but was cut down due to computational costs on cluster!
    auto s3dft_ef = impl::s3dft_exp_fn(n);
    const auto s3dft_measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, s3dft_ef, experiment_count);
    const auto s3dft_result = tixl::mpi_compute_statistics(MPI_COMM_WORLD, s3dft_measurements);
    if (world_rank == 0)
    {
        tixl::output_results("Results of the runs with S3DFT", s3dft_result, 3 * 8 * n * n * n * n);
        benchmarking::write_measurements(s3dft_measurements, std::string("s3dft_measurements_p") + std::to_string(p)
            + std::string(".txt"));
    }
    
    // finalize MPI
    MPI_Finalize();
    
    return 0;
}
