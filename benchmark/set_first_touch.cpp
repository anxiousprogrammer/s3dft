//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation of 'set_first_touch.h'

#include "set_first_touch.h"

#include <omp.h>

#include <utility>
#include <iostream>

#include <unistd.h> // sysconf

#include <immintrin.h> // intrinsics

namespace s3dft
{
namespace benchmarking
{

//+//////////////////////////////////////////////
// implementation
//+//////////////////////////////////////////////

constexpr static unsigned g_cache_line_size = S3DFT_CACHE_LINE_SIZE;

template<class Treal_t>
void set_first_touch(tensor<Treal_t>& tens, const std::vector<std::size_t>& work_indices)
{
    if (!__builtin_cpu_supports("sse2"))
    {
        std::cerr << "Warning! The CPU does not have the \'sse2\' flag. Forcing the eviction of cache lines to memory might not "
            "work." << std::endl;
    }
    
    const auto page_size = sysconf(_SC_PAGESIZE) / sizeof(complex<Treal_t>);
    if (page_size == 0)
    {
        throw std::runtime_error("set_first_touch_page_size_zero_error");
    }
    
    for (const auto& index : work_indices)
    {
        auto* data_begin = tens.get_slice_data(index);
        for (auto page_index = std::size_t{}; page_index < tens.get_slice_size(); page_index += page_size)
        {
            // touch
            data_begin[page_index].real = Treal_t{};
            
            // evict
            #ifdef S3DFT_MBENCH_EVICT_CACHE_LINES
            constexpr static unsigned cache_line_size_complex = g_cache_line_size / sizeof(complex<Treal_t>);
            for (auto cache_line_index = std::size_t{}; cache_line_index < page_size;
                cache_line_index += cache_line_size_complex)
            {
                _mm_clflush(data_begin + page_index + cache_line_index);
            }
            #endif
        }
    }
}

template<class Treal_t>
void set_first_touch(matrix<Treal_t>& mat)
{
    if (!__builtin_cpu_supports("sse2"))
    {
        std::cerr << "Warning! The CPU does not have the \'sse2\' flag. Forcing the eviction of cache lines to memory might not "
            "work." << std::endl;
    }
    
    const auto page_size = sysconf(_SC_PAGESIZE) / sizeof(complex<Treal_t>);
    if (page_size == 0)
    {
        throw std::runtime_error("set_first_touch_page_size_zero_error");
    }
    
    auto* data_begin = mat.data();
    for (auto page_index = std::size_t{}; page_index < mat.size(); page_index += page_size)
    {
        // touch
        data_begin[page_index].real = Treal_t{};
        
        // evict
        #ifdef S3DFT_MBENCH_EVICT_CACHE_LINES
        constexpr static unsigned cache_line_size_complex = g_cache_line_size / sizeof(complex<Treal_t>);
        for (auto cache_line_index = std::size_t{}; cache_line_index < page_size;
            cache_line_index += cache_line_size_complex)
        {
            _mm_clflush(data_begin + page_index + cache_line_index);
        }
        #endif
    }
}


//+//////////////////////////////////////////////
// explicit instantiation
//+//////////////////////////////////////////////

template void set_first_touch<float>(tensor<float>&, const std::vector<std::size_t>&);
template void set_first_touch<double>(tensor<double>&, const std::vector<std::size_t>&);

template void set_first_touch<float>(matrix<float>&);
template void set_first_touch<double>(matrix<double>&);

} // namespace benchmarking
} // namespace s3dft
