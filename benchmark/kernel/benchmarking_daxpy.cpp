//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of the DAXPY loop.

#include "implementation/schedule_thread_work.h"
#include "tensor.h"

#include "../set_first_touch.h"

#include "tixl.h"

#include <omp.h>

#include <memory> // std::unique_ptr
#include <utility> // std::abort
#include <iostream> // std::cerr

#include <unistd.h> // sysconf


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// Functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t n_;
    double lambda_;
    tensor<double>* tens_a_ = nullptr;
    tensor<double>* tens_b_ = nullptr;

    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& n, const double& lambda)
        : n_(n), lambda_(lambda) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate
        tens_a_ = new tensor<double>(n_);
        tens_b_ = new tensor<double>(n_);
        if (tens_a_ == nullptr || tens_b_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_a_->get_n(), 0);
            benchmarking::set_first_touch(*tens_a_, work_indices);
            benchmarking::set_first_touch(*tens_b_, work_indices);
        }
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_a_->get_n(), 0);
            for (const auto& slice_index : work_indices)
            {
                auto* slice_a = reinterpret_cast<double*>(tens_a_->get_slice_data(slice_index));
                auto* slice_b = reinterpret_cast<double*>(tens_b_->get_slice_data(slice_index));
                const auto slice_size = 2 * tens_a_->get_slice_size(); // 2 doubles for a complex
                #pragma omp simd safelen(8)
                for (auto index = std::size_t{}; index < slice_size; ++index)
                {
                    slice_b[index] += lambda_ * slice_a[index]; // fp_count: 2, traffic: 2+1
                }
            }
        }
    }
    
    void finish() final
    {
        delete tens_a_;
        delete tens_b_;
    }
};

} // namespace impl


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument" << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 10 || n > 1500)
    {
        std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size N=" << n << ".\nThread count: " << omp_get_max_threads() << ".\n";
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the schoenauer vector triad experiment
    auto ef2 = impl::exp_fn(n, 10.0);
    const auto measurements = tixl::perform_experiments(ef2, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of DAXPY loop", stats, 2 * 2 * n * n * n, 3 * n * n * n * sizeof(complex<double>));
    
    return 0;
}

// END OF TESTFILE
