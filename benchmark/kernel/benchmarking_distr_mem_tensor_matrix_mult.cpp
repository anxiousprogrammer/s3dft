//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of 'distr_mem::tensor_matrix_mult'.

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "implementation/tensor_matrix_math.h"
#include "matrix.h"
#include "tensor.h"

#include "../set_first_touch.h"
#include "../write_measurements.h"
#include "message_packet_details.h"

#include "tixl_w_mpi.h"

#include "mkl.h"

#include <mpi.h>
#include <omp.h>

#include <iostream> // std::cout
#include <stdexcept> // std::invalid_argument
#include <cmath> // std::cbrt
#include <string> // std::stoul


using namespace s3dft;

template<class Treal_t>
void tensor_matrix_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, tensor<Treal_t>& tens_block,
    matrix<Treal_t>& mat_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices)
{
    // pre-conditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (p < 1)
        {
            throw std::invalid_argument("tensor_matrix_mult_invalid_p_error");
        }
        if (tens_block.get_n() != matrix.get_n())
        {
            throw std::invalid_argument("tensor_matrix_mult_tensor_matrix_size_mismatch_error");
        }
        if (tens_block.size() != scratch_tens.size())
        {
            throw std::invalid_argument("tensor_matrix_mult_scratch_tensor_size_error");
        }
        if (mat_block.size() != scratch_mat.size())
        {
            throw std::invalid_argument("tensor_matrix_mult_scratch_matrix_size_error");
        }
    }
    #endif
    
    // obtain proc. coordinates
    const auto [bi1, bi2, bi3] = get_proc_coords(p, proc_rank);
    
    // thread-num
    const auto thread_num = omp_get_thread_num();
    
    // constant: MPI data-type
    static constexpr auto mpi_datatype = std::is_same<Treal_t, double>::value ? MPI_DOUBLE : MPI_FLOAT;
    
    // message-packet size
    auto mpd = benchmarking::impl::message_packet_details{};
    static auto requests = std::array<MPI_Request, 5000>{}; // NOTE: HARD LIMIT HERE!
    if (thread_num == COMM_THREAD)
    {
        // calculate the details
        benchmarking::impl::get_message_packet_details<Treal_t>(tens_block.size(), mpd);
    }
    const auto packet_count_times_2 = 2 * mpd.packet_count;
    const auto requests_size = packet_count_times_2 + 2;
    
    // re-arrange the data before starting
    if (thread_num == COMM_THREAD)
    {
        if (bi2 != 0)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, bi2, (bi3 + p + bi2) % p); // from right
            const auto dest_rank = get_proc_id(p, bi1, bi2, (bi3 + p - bi2) % p); // towards left
            for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
            {
                const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                const auto irecv_res = MPI_Irecv(scratch_tens.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    src_rank, 0, comm, &requests[2 * packet_index]);
                const auto isend_res = MPI_Isend(tens_block.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    dest_rank, 0, comm, &requests[2 * packet_index + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                }
            }
        }
        if (bi3 != 0)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, (bi2 + p + bi3) % p, bi3); // from below
            const auto dest_rank = get_proc_id(p, bi1, (bi2 + p - bi3) % p, bi3); // towards up
            const auto irecv_res = MPI_Irecv(scratch_mat.data(), 2 * scratch_mat.size(), mpi_datatype, src_rank, 1, comm,
                &requests[packet_count_times_2]);
            const auto isend_res = MPI_Isend(mat_block.data(), 2 * mat_block.size(), mpi_datatype, dest_rank, 1, comm,
                &requests[packet_count_times_2 + 1]);
            if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
            }
        }
        else
        {
            // copy the current data into the send buffer
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), scratch_mat.data());
        }
        
        // waiting...
        if (bi2 != 0 && bi3 == 0) // tensor only
        {
            if (MPI_SUCCESS != MPI_Waitall(packet_count_times_2, requests.data(), MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
            }
        }
        else if (bi2 == 0 && bi3 != 0) // matrix only
        {
            if (MPI_SUCCESS != MPI_Waitall(2, requests.data() + packet_count_times_2, MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
            }
        }
        else if (bi3 != 0 && bi2 != 0) // matrix and tensor
        {
            if (MPI_SUCCESS != MPI_Waitall(requests_size, requests.data(), MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
            }
        }
    }
    if (bi2 == 0 && thread_num > COMM_THREAD)
    {
        // copy the current data into the send buffer
        tensor_copy(tens_block, scratch_tens, work_indices);
    }
    #pragma omp barrier

    // mapping
    const auto dest_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p - 1) % p); // towards left
    const auto dest_rank_mat = get_proc_id(p, bi1, (bi2 + p - 1) % p, bi3); // towards up
    const auto src_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p + 1) % p); // from right
    const auto src_rank_mat = get_proc_id(p, bi1, (bi2 + p + 1) % p, bi3); // from below
    
    // loop
    for (auto index = std::size_t{}; index < p; ++index)
    {
        // renaming for readability
        const auto buffer_selection_condition = index % 2 != 0;
        auto& tens_input = buffer_selection_condition ? tens_block : scratch_tens;
        auto& tens_recv_buffer = buffer_selection_condition ? scratch_tens : tens_block;
        auto& mat_input = buffer_selection_condition ? mat_block : scratch_mat;
        auto& mat_recv_buffer = buffer_selection_condition ? scratch_mat : mat_block;
        
        if (thread_num == COMM_THREAD)
        {
            if (index < p-1)
            {
                for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
                {
                    const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                    const auto irecv_res = MPI_Irecv(tens_recv_buffer.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, src_rank_tens, 2, comm, &requests[2 * packet_index]);
                    const auto isend_res = MPI_Isend(tens_input.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, dest_rank_tens, 2, comm, &requests[2 * packet_index + 1]);
                    if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                    {
                        throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                    }
                }
                const auto irecv_res = MPI_Irecv(mat_recv_buffer.data(), 2 * mat_recv_buffer.size(), mpi_datatype, src_rank_mat, 3,
                    comm, &requests[packet_count_times_2]);
                const auto isend_res = MPI_Isend(mat_input.data(), 2 * mat_input.size(), mpi_datatype, dest_rank_mat, 3, comm,
                    &requests[packet_count_times_2 + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                }
                
                // waiting...
                if (MPI_SUCCESS != MPI_Waitall(requests_size, requests.data(), MPI_STATUSES_IGNORE))
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
                }
            }
        }
        else
        {
            // compute the product
            ::s3dft::shared_mem::tensor_matrix_mult(tens_input, mat_input, result_block, work_indices);
        }
        #pragma omp barrier
    }
}


namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t p_;
    const std::size_t b_;
    const int world_rank_;
    
    tensor<double>* operand0_ = nullptr;
    matrix<double>* operand1_ = nullptr;
    tensor<double>* result_ = nullptr;
    tensor<double>* scratch0_ = nullptr;
    matrix<double>* scratch1_ = nullptr;
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& p, const std::size_t& b, const int world_rank)
        : p_(p), b_(b), world_rank_(world_rank) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocation
        operand0_ = new tensor<double>(b_);
        operand1_ = new matrix<double>(b_);
        result_ = new tensor<double>(b_);
        scratch0_ = new tensor<double>(b_);
        scratch1_ = new matrix<double>(b_);
        if (operand0_ == nullptr
            || operand1_ == nullptr
            || result_ == nullptr
            || scratch0_ == nullptr
            || scratch1_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        }
        
        // first-touch
        #pragma omp parallel
        {
            if (omp_get_thread_num() != COMM_THREAD)
            {
                const auto work_indices = schedule_thread_work(operand0_->get_n(), 1);
                benchmarking::set_first_touch(*operand0_, work_indices);
                benchmarking::set_first_touch(*result_, work_indices);
                benchmarking::set_first_touch(*scratch0_, work_indices);
            }
        }
        benchmarking::set_first_touch(*operand1_);
        benchmarking::set_first_touch(*scratch1_);
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(operand0_->get_n(), 1);
            tensor_matrix_mult(p_, MPI_COMM_WORLD, world_rank_, *operand0_, *operand1_, *result_, *scratch0_, *scratch1_,
                work_indices);
        }
    }
    
    void finish() final
    {
        // force reallocation
        delete operand0_;
        delete operand1_;
        delete result_;
        delete scratch0_;
        delete scratch1_;
    }
};


} // namespace impl


int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////

    auto provided_thread_support = int{-1};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided_thread_support);
    if (provided_thread_support < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    const auto p_as_fp = std::cbrt(static_cast<double>(world_size));
    const auto p = static_cast<std::size_t>(p_as_fp);
    if (std::fabs(static_cast<double>(p) - p_as_fp) > 1E-15)
    {
        std::cerr << "Fatal error: the number of processes must be cubic." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    //+//////////////////////////////////////////
    // extract problem size
    //+//////////////////////////////////////////
    
    if (argc < 2)
    {
        if (world_rank == 0)
        {
            std::cerr << "Please provide the problem size as command line argument" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    const auto b = n / p;
    if (n < 10 || b > 5000)
    {
        if (world_rank == 0)
        {
            std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    
    //+//////////////////////////////////////////
    // benchmark runs
    //+//////////////////////////////////////////
    
    // progess
    if (world_rank == 0)
    {
        std::cout << "Starting runs with N=" << n << ", N_node=" << world_size
            << ".\nThread count: " << omp_get_max_threads() << ".\n";
    }
    
    // perform the experiments
    const auto experiment_count = std::size_t{100};
    auto ef = impl::exp_fn(p, b, world_rank);
    const auto measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, ef, experiment_count);
    const auto stats = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements);
    if (world_rank == 0)
    {
        tixl::output_results("Benchmarking of distr. mem. tensor-matrix mult.", stats, 8 * n * n * n * n);
        benchmarking::write_measurements(measurements, "benchmarking_distr_mem_tensor_matrix_mult_measurements_p"
            + std::to_string(p) + ".txt");
    }
    
    // Finalize
    MPI_Finalize();
    
    return 0;
}
