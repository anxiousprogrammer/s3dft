//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of 'shared_mem::tensor_matrix_mult'.

#include "implementation/schedule_thread_work.h"
#include "matrix.h"
#include "tensor.h"

#include "../set_first_touch.h"
#include "tixl.h"

#include "mkl.h"

#include <omp.h>

#include <iostream> // std::cout
#include <utility> // std::size_t
#include <string> // std::stoul
#include <cstdlib> // std::abort


using namespace s3dft;

namespace impl
{

template<class Treal_t>
void cblas_xgemm(const complex<Treal_t>* m1, const complex<Treal_t>* m2, complex<Treal_t>* m3, const std::size_t& n) noexcept
{
    if constexpr (std::is_same<Treal_t, double>::value)
    {
        const auto alpha_beta = complex<double>{1.0, 0.0};
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, static_cast<const void*>(&alpha_beta),
            static_cast<const void*>(m1), n, static_cast<const void*>(m2), n, static_cast<const void*>(&alpha_beta),
            static_cast<void*>(m3), n);
    }
    else if constexpr (std::is_same<Treal_t, float>::value)
    {
        const auto alpha_beta = complex<float>{1.f, 0.f};
        cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, static_cast<const void*>(&alpha_beta),
            static_cast<const void*>(m1), n, static_cast<const void*>(m2), n, static_cast<const void*>(&alpha_beta),
            static_cast<void*>(m3), n);
    }
}

} // namespace impl

// tensor-matrix multiplication with slices along direction 1
template<class Treal_t>
void tensor_matrix_mult(const tensor<Treal_t>& tens, const matrix<Treal_t>& mat, tensor<Treal_t>& result,
    const std::vector<std::size_t>& work_indices)
{
    // preconditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (tens.get_n() != mat.get_n() || tens.get_n() != result.get_n())
        {
            throw std::invalid_argument("shared_mem_tensor_matrix_mult_input_size_mismatch_error");
        }
    }
    #endif
    
    for (const auto& index : work_indices)
    {
        const auto* left_mat = tens.get_slice_data(index);
        auto* result_mat = result.get_slice_data(index);
        impl::cblas_xgemm(left_mat, mat.data(), result_mat, tens.get_n());
    }
}


namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // Members
    //+/////////////////
    
    const std::size_t n_;
    tensor<double>* operand0_ = nullptr;
    matrix<double>* operand1_ = nullptr;
    tensor<double>* result_ = nullptr;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& n)
        : n_(n) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate exp. data
        operand0_ = new tensor<double>(n_);
        operand1_ = new matrix<double>(n_);
        result_ = new tensor<double>(n_);
        if (operand0_ == nullptr
            || operand1_ == nullptr
            || result_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(operand0_->get_n(), 0);
            benchmarking::set_first_touch(*operand0_, work_indices);
            benchmarking::set_first_touch(*result_, work_indices);
        }
        benchmarking::set_first_touch(*operand1_);
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(operand0_->get_n(), 0);
            tensor_matrix_mult(*operand0_, *operand1_, *result_, work_indices);
        }
    }
    
    void finish() final
    {
        delete operand0_;
        delete operand1_;
        delete result_;
    }
};

} // namespace impl


int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument" << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 10 || n > 1500)
    {
        std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // progess
    std::cout << "Starting runs with N=" << n << ".\nThread count: " << omp_get_max_threads() << ".\n";
    
    // perform experiments
    const auto experiment_count = std::size_t{100};
    auto ef = impl::exp_fn(n);
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto result = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of shared mem. tensor-matrix mult.", result, n * n * n * n * 8,
        2 * n * n * n * (n + 1) * sizeof(complex<double>));
    
    return 0;
}
