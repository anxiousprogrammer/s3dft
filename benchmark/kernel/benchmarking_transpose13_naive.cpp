//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of the naive transpose13 function.

#include "implementation/schedule_thread_work.h"
#include "tensor.h"

#include "../set_first_touch.h"

#include "tixl.h"

#include <omp.h>

#include <iostream> // std::cout
#include <string> // std::stoul
#include <cstdlib> // std::abort


using namespace s3dft;

template<class Treal_t>
void transpose13(const tensor<Treal_t>& tens, tensor<Treal_t>& tens_tr)
{
    // preconditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (tens.get_n() == 0)
        {
            throw std::invalid_argument("transpose13_tensor_size_error");
        }
        if (tens_tr.get_n() == tens.get_n())
        {
            throw std::invalid_argument("transpose13_tensor_size_mismatch_error");
        }
    }
    #endif
    
    // renaming for convenience
    const auto& n = tens.get_n();
    #pragma omp for schedule(static)
    for (auto i2 = std::size_t{}; i2 < n; ++i2)
    {
        for (auto i1 = std::size_t{}; i1 < n; ++i1)
        {
            for (auto i3 = std::size_t{}; i3 < n; ++i3)
            {
                tens_tr(i3, i2, i1) = tens(i1, i2, i3);
            }
        }
    }
}

namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t& n_;
    tensor<double>* operand_ = nullptr;
    tensor<double>* result_ = nullptr;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& n)
        : n_(n) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocation
        operand_ = new tensor<double>(n_);
        result_ = new tensor<double>(n_);
        if (operand_ == nullptr || result_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(operand_->get_n(), 1);
            benchmarking::set_first_touch(*operand_, work_indices);
            benchmarking::set_first_touch(*result_, work_indices);
        }
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            transpose13(*operand_, *result_);
        }
    }
    
    void finish() final
    {
        delete operand_;
        delete result_;
    }
};

} // namespace impl


int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument" << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 10 || n > 1500)
    {
        std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }

    // progess
    std::cout << "Starting with the runs [N=" << n << "].\nThread count: " << omp_get_max_threads() << ".\n";
    
    // perform the experiments (1)
    const auto experiment_count = std::size_t{100};
    auto ef = impl::exp_fn(n);
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto results = tixl::compute_statistics(measurements);
    tixl::output_results("Naive transpose13", results, 0, n * n * n * 3 * sizeof(complex<double>));
        
    return 0;
}
