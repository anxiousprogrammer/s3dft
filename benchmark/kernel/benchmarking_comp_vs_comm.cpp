//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of the overlapping of communication and computation in
/// 'distr_mem::tensor_matrix_math'.

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "implementation/tensor_matrix_math.h"
#include "matrix.h"
#include "tensor.h"

#include "../set_first_touch.h"
#include "../write_measurements.h"
#include "message_packet_details.h"

#include "tixl_w_mpi.h"

#include <omp.h>
#include <mpi.h>

#include <iostream> // std::cout
#include <stdexcept> // std::invalid_argument
#include <cmath> // std::cbrt
#include <string> // std::stoul


using namespace s3dft;

namespace impl
{

class comm_and_comp : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t b_;
    
    tensor<double>* tens_recv_buffer_ = nullptr;
    tensor<double>* tens_send_buffer_ = nullptr;
    matrix<double>* mat_recv_buffer_ = nullptr;
    matrix<double>* mat_send_buffer_ = nullptr;
    tensor<double>* result_ = nullptr;
    
    std::size_t dest_rank_tens_ = {};
    std::size_t dest_rank_mat_ = {};
    std::size_t src_rank_tens_ = {};
    std::size_t src_rank_mat_ = {};
    
    benchmarking::impl::message_packet_details mpd_ = {};
    std::size_t packet_count_times_2_ = {};
    std::vector<MPI_Request> requests_ = {};
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    comm_and_comp(const std::size_t& p, const std::size_t& b, const int proc_rank)
        : b_(b)
    {
        // calculation of target and source ranks
        const auto [bi1, bi2, bi3] = get_proc_coords(p, proc_rank);
        dest_rank_tens_ = get_proc_id(p, bi1, bi2, (bi3 + p - 1) % p); // towards left
        dest_rank_mat_ = get_proc_id(p, bi1, (bi2 + p - 1) % p, bi3); // towards up
        src_rank_tens_ = get_proc_id(p, bi1, bi2, (bi3 + p + 1) % p); // from right
        src_rank_mat_ = get_proc_id(p, bi1, (bi2 + p + 1) % p, bi3); // from below
        
        // calculate the details
        ::s3dft::benchmarking::impl::get_message_packet_details<double>(b * b * b, mpd_);
        
        // create a request array (including requests for the communication of the block matrix)
        requests_.resize(2 * (mpd_.packet_count + 1));
        packet_count_times_2_ = 2 * mpd_.packet_count;
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocation
        tens_recv_buffer_ = new tensor<double>(b_);
        tens_send_buffer_ = new tensor<double>(b_);
        mat_recv_buffer_ = new matrix<double>(b_);
        mat_send_buffer_ = new matrix<double>(b_);
        result_ = new tensor<double>(b_);
        if (tens_recv_buffer_ == nullptr || tens_send_buffer_ == nullptr || mat_recv_buffer_ == nullptr
            || mat_send_buffer_ ==  nullptr || result_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        }
        
        // first-touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_recv_buffer_->get_n(), 1);
            benchmarking::set_first_touch(*tens_recv_buffer_, work_indices);
            benchmarking::set_first_touch(*tens_send_buffer_, work_indices);
            benchmarking::set_first_touch(*result_, work_indices);
        }
        benchmarking::set_first_touch(*mat_recv_buffer_);
        benchmarking::set_first_touch(*mat_send_buffer_);
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            if (omp_get_thread_num() == COMM_THREAD)
            {
                for (auto packet_index = std::size_t{}; packet_index < mpd_.packet_count; ++packet_index)
                {
                    const auto message_size = 2 * (packet_index < mpd_.packet_count - 1 ? mpd_.packet_size
                        : mpd_.rest_packet_size);
                    const auto irecv_res = MPI_Irecv(tens_recv_buffer_->data() + packet_index * mpd_.packet_size, message_size,
                        MPI_DOUBLE, src_rank_tens_, 2, MPI_COMM_WORLD, &requests_[2 * packet_index]);
                    const auto isend_res = MPI_Isend(tens_send_buffer_->data() + packet_index * mpd_.packet_size, message_size,
                        MPI_DOUBLE, dest_rank_tens_, 2, MPI_COMM_WORLD, &requests_[2 * packet_index + 1]);
                    if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                    {
                        throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                    }
                }
                const auto irecv_res = MPI_Irecv(mat_recv_buffer_->data(), 2 * mat_recv_buffer_->size(), MPI_DOUBLE,
                    src_rank_mat_, 3, MPI_COMM_WORLD, &requests_[packet_count_times_2_]);
                const auto isend_res = MPI_Isend(mat_send_buffer_->data(), 2 * mat_send_buffer_->size(), MPI_DOUBLE, dest_rank_mat_,
                    3, MPI_COMM_WORLD, &requests_[packet_count_times_2_ + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                }
                
                // waiting...
                if (MPI_SUCCESS != MPI_Waitall(requests_.size(), requests_.data(), MPI_STATUSES_IGNORE))
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
                }
            }
            else
            {
                const auto work_indices = schedule_thread_work(tens_recv_buffer_->get_n(), 1);
                shared_mem::tensor_matrix_mult(*tens_send_buffer_, *mat_send_buffer_, *result_, work_indices);
            }
        }
    }
    
    void finish() final
    {
        // force reallocation
        delete tens_recv_buffer_;
        delete tens_send_buffer_;
        delete mat_recv_buffer_;
        delete mat_send_buffer_;
        delete result_;
    }
};

} // namespace impl


int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////

    auto provided_thread_support = int{-1};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided_thread_support);
    if (provided_thread_support < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    const auto p_as_fp = std::cbrt(static_cast<double>(world_size));
    const auto p = static_cast<std::size_t>(p_as_fp);
    if (std::fabs(static_cast<double>(p) - p_as_fp) > 1E-15)
    {
        std::cerr << "Fatal error: the number of processes must be cubic." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    //+//////////////////////////////////////////
    // extract problem size
    //+//////////////////////////////////////////
    
    if (argc < 2)
    {
        if (world_rank == 0)
        {
            std::cerr << "Please provide the block size as command line argument" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    const auto b = static_cast<std::size_t>(std::stoul(argv[1]));
    if (b < 10 || b > 5000)
    {
        if (world_rank == 0)
        {
            std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    
    //+//////////////////////////////////////////
    // benchmark runs of communication/computation
    // overlap
    //+//////////////////////////////////////////
    
    // progess
    if (world_rank == 0)
    {
        std::cout << "Starting runs for communication/computation overlap with b=" << b << ", N_node=" << world_size
            << ".\nThread count: " << omp_get_max_threads() << ".\n";
    }
    
    // perform the experiments
    const auto experiment_count = std::size_t{100};
    auto comp_and_comm_ef = impl::comm_and_comp(p, b, world_rank);
    const auto comp_and_comm_measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, comp_and_comm_ef, experiment_count);
    const auto comp_and_comm_stats = tixl::mpi_compute_statistics(MPI_COMM_WORLD, comp_and_comm_measurements);
    if (world_rank == 0)
    {
        tixl::output_results("Benchmarking of communication/computation overlap", comp_and_comm_stats);
    }
    
    // end
    MPI_Finalize();
    
    return 0;
}
