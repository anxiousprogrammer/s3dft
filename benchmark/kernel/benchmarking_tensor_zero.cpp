//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the benchmark program of the tensor copy operation.

#include "implementation/schedule_thread_work.h"
#include "tensor.h"

#include "../set_first_touch.h"

#include "tixl.h"

#include <omp.h>

#include <memory> // std::unique_ptr
#include <utility> // std::abort
#include <iostream> // std::cerr

#include <unistd.h> // sysconf


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// Functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t n_;
    tensor<double>* tens_ = nullptr;

    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& n)
        : n_(n) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate
        tens_ = new tensor<double>(n_);
        if (tens_ == nullptr)
        {
            std::cerr << "Fatal error: test data could not be allocated for!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_->get_n(), 0);
            benchmarking::set_first_touch(*tens_, work_indices);
        }
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_->get_n(), 0);
            for (const auto& index : work_indices)
            {
                auto* data_begin = tens_->get_slice_data(index);
                auto* data_end = data_begin + tens_->get_slice_size();
                std::fill(data_begin, data_end, complex<double>{});
            }
        }
    }
    
    void finish() final
    {
        delete tens_;
        tens_ = nullptr;
    }
};

} // namespace impl


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument" << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 10 || n > 1500)
    {
        std::cerr << "Invalid problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size N=" << n << ".\nThread count: " << omp_get_max_threads() << ".\n";
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the schoenauer vector triad experiment
    auto ef2 = impl::exp_fn(n);
    const auto measurements = tixl::perform_experiments(ef2, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of tensor zero operation", stats, 0, 2 * n * n * n * sizeof(complex<double>));
    
    return 0;
}

// END OF TESTFILE
