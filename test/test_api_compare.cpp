//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains tests which compare the output of S3DFT's API function with that of FFTW3.

#include "implementation/proc_plan.h"
#include "implementation/schedule_thread_work.h"
#include "distr_mem_3d_dft.h"

#include "utility/mpi_test_asserts.h"
#include "utility/core_count.h"

#include "fftw/fftw3.h"
#include "fftw/fftw3-mpi.h"

#include <mpi.h>
#include <omp.h>

#include <cstdlib> // std::rand
#include <cmath> // std::sin

using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// helper functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

s3dft::tensor<double> get_random_data(const std::size_t& n)
{
    auto result = s3dft::tensor<double>(n);
    for (auto index = std::size_t{}; index < result.size(); ++index)
    {
        const auto rand_value = []()
        {
            return (std::rand() % 2 == 0 ? 1.0 : -1.0) * std::sin(static_cast<double>(std::rand() % 10 + 1));
        };
        result.data()[index] = {rand_value(), rand_value()}; // FFTK only has an R2C transform
    }
    
    return result;
}

void scatter_data(const s3dft::tensor<double> global_data, const int local_0_start, const int local_n0, double* local_data,
    const int world_rank, const int world_size)
{
    // gather local sizes
    auto local_sizes = std::vector<int>();
    if (world_rank == 0)
    {
        local_sizes = std::vector<int>(world_size, 0);
    }
    const auto n = static_cast<int>(global_data.get_n());
    const auto local_size = static_cast<int>(sizeof(fftw_complex) / sizeof(double)) * local_n0 * n * n;
    MPI_Gather(&local_size, 1, MPI_INT, local_sizes.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    // compute offsets
    auto offsets = std::vector<int>();
    if (world_rank == 0)
    {
        offsets = std::vector<int>(world_size, 0);
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            offsets[index] = offsets[index - 1] + local_sizes[index - 1];
        }
    }
    
    // scatter dat
    MPI_Scatterv(global_data.data(), local_sizes.data(), offsets.data(), MPI_DOUBLE, local_data, local_size, MPI_DOUBLE, 0,
        MPI_COMM_WORLD);
}

void gather_data(const double* local_data, const int local_0_start, const int local_n0, s3dft::tensor<double>& gathered_data,
    const int world_rank, const int world_size)
{
    // gather local sizes
    auto local_sizes = std::vector<int>();
    if (world_rank == 0)
    {
        local_sizes = std::vector<int>(world_size, 0);
    }
    const auto n = static_cast<int>(gathered_data.get_n());
    const auto fftw_complex_size_factor = static_cast<int>(sizeof(fftw_complex) / sizeof(double));
    const auto local_size = fftw_complex_size_factor * local_n0 * n * n;
    MPI_Gather(&local_size, 1, MPI_INT, local_sizes.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    // compute offsets
    auto offsets = std::vector<int>();
    if (world_rank == 0)
    {
        offsets = std::vector<int>(world_size, 0);
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            offsets[index] = offsets[index - 1] + local_sizes[index - 1];
        }
    }
    
    // gather output data
    MPI_Gatherv(local_data, local_size, MPI_DOUBLE, gathered_data.data(), local_sizes.data(), offsets.data(), MPI_DOUBLE,
        0, MPI_COMM_WORLD);
}

void scatter_volumetric_data(const s3dft::tensor<double> global_data, s3dft::tensor<double>& local_data, const int world_rank,
    const int world_size)
{
    const auto p = static_cast<std::size_t>(std::cbrt(static_cast<double>(world_size)));
    const auto b = global_data.get_n() / p;
    if (world_rank == 0)
    {
        // perform domain decomposition of tens_block_domain
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    // copy the data of the block domain into a container
                    auto block_sendbuf = s3dft::tensor<double>(b);
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                block_sendbuf(i1, i2, i3) = global_data(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                            }
                        }
                    }
                    
                    // in case of the root process
                    const auto target_proc_id = get_proc_id(p, bi1, bi2, bi3);
                    if (target_proc_id == 0)
                    {
                        std::copy(block_sendbuf.data(), block_sendbuf.data() + block_sendbuf.size(), local_data.data());
                        continue;
                    }
                    
                    // send the tensor's block domain to its associated process
                    MPI_Send(block_sendbuf.data(), sizeof(s3dft::complex<double>) / sizeof(double) * block_sendbuf.size(),
                        MPI_DOUBLE, target_proc_id, 0, MPI_COMM_WORLD);
                }
            }
        }
    }
    else
    {
        // start obtaining m1's block domain
        MPI_Recv(local_data.data(), sizeof(s3dft::complex<double>) / sizeof(double) * local_data.size(), MPI_DOUBLE, 0, 0,
            MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
}

void gather_volumetric_data(const s3dft::tensor<double>& local_data, s3dft::tensor<double>& global_data, const int world_rank,
    const int world_size)
{
    // details
    const auto p = static_cast<std::size_t>(std::cbrt(static_cast<double>(world_size)));
    const auto b = global_data.get_n() / p;
    
    // send/receive into an array of blocks
    if (world_rank == 0)
    {
        // gather back the data
        auto received_tensors = std::vector(world_size, s3dft::tensor<double>(b));
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(local_data.get_n(), 1);
            tensor_copy(local_data, received_tensors[0], work_indices);
        }
        auto requests = std::vector(world_size - 1, MPI_Request());
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            MPI_Irecv(received_tensors[index].data(),
                sizeof(s3dft::complex<double>) / sizeof(double) * received_tensors[index].size(), MPI_DOUBLE, index, 0,
                MPI_COMM_WORLD, &requests[index - 1]);
        }
        MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE);
        
        // copy into output container
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    const auto relevant_result_block = received_tensors[get_proc_id(p, bi1, bi2, bi3)];
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                global_data(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3) = relevant_result_block(i1, i2, i3);
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        MPI_Send(local_data.data(), sizeof(s3dft::complex<double>) / sizeof(double) * local_data.size(), MPI_DOUBLE, 0, 0,
            MPI_COMM_WORLD);
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// tester function
//+////////////////////////////////////////////////////////////////////////////////////////////////

void run_test(const std::ptrdiff_t& n, const double& tolerance, const int world_rank, const int world_size)
{
    //+/////////////////
    // get random data
    //+/////////////////
    
    auto global_data = s3dft::tensor<double>(n);
    if (world_rank == 0)
    {
        global_data = get_random_data(n);
    }
    
    //+/////////////////
    // FFTW3
    //+/////////////////
    
    // size of decomposition
    auto local_n0 = ptrdiff_t{};
    auto local_0_start = ptrdiff_t{};
    const auto fftw3_local_size = fftw_mpi_local_size_3d(n, n, n, MPI_COMM_WORLD, &local_n0, &local_0_start);
    if (fftw3_local_size == 0)
    {
        std::cerr << "Fatal error: FFTW calculated a local size of 0 (domain decomposition failure)." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // allocate data
    auto* fftw3_local_data = fftw_alloc_complex(fftw3_local_size);
    if (fftw3_local_data == nullptr)
    {
        std::cerr << "Fatal error: test data could not be allocated for." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
    }
    
    // create a plan
    auto fftw3_plan = fftw_mpi_plan_dft_3d(n, n, n, fftw3_local_data, fftw3_local_data, MPI_COMM_WORLD, FFTW_FORWARD,
         FFTW_ESTIMATE);
    if (fftw3_plan == NULL)
    {
        std::cerr << "Fatal error: fftw_plan could not be created." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
    }
    
    // init data
    std::fill(reinterpret_cast<double*>(fftw3_local_data), reinterpret_cast<double*>(fftw3_local_data + fftw3_local_size), 1.0);
    scatter_data(global_data, local_0_start, local_n0, reinterpret_cast<double*>(fftw3_local_data), world_rank, world_size);
    
    // run dat
    fftw_execute(fftw3_plan);
    
    //+/////////////////
    // S3DFT
    //+/////////////////
    
    // allocate input and scratch
    const auto b = compute_block_size(n, MPI_COMM_WORLD);
    auto s3dft_local_data = s3dft::tensor<double>(b);
    const auto coeff_block = create_coeff_block_matrix<double>(n, b, MPI_COMM_WORLD);
    auto scache = create_scratch_cache<double>(b);
    
    // init local data
    scatter_volumetric_data(global_data, s3dft_local_data, world_rank, world_size);
    
    // run
    execute(n, s3dft_local_data, coeff_block, scache, MPI_COMM_WORLD);
    
    //+/////////////////
    // gather to root
    //+/////////////////
    
    // FFTW3
    auto fftw3_gathered_output = s3dft::tensor<double>(n);
    gather_data(reinterpret_cast<const double*>(fftw3_local_data), local_0_start, local_n0, fftw3_gathered_output, world_rank,
        world_size);
    
    // S3DFT
    auto s3dft_gathered_output = s3dft::tensor<double>(n);
    gather_volumetric_data(s3dft_local_data, s3dft_gathered_output, world_rank, world_size);
    
    //+/////////////////
    // validate
    //+/////////////////
    
    if (world_rank == 0)
    {
        for (auto index = std::size_t{}; index < s3dft_gathered_output.size(); ++index)
        {
            mpi_assert_fp_equal(fftw3_gathered_output.data()[index].real, s3dft_gathered_output.data()[index].real, tolerance);
            mpi_assert_fp_equal(fftw3_gathered_output.data()[index].imag, s3dft_gathered_output.data()[index].imag, tolerance);
        }
    }
    
    //+/////////////////
    // cleanup
    //+/////////////////
    
    fftw_destroy_plan(fftw3_plan);
    fftw_free(fftw3_local_data);
}

int main(int argc, char** argv)
{
    //+/////////////////
    // init
    //+/////////////////
    
    // MPI init
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // set the number of OpenMP threads
    const auto thread_count = test::g_core_count;
    omp_set_num_threads(thread_count);
    
    // init threads for fftw
    if (fftw_init_threads() == 0)
    {
        std::cerr << "Fatal error: FFTW could not initialize threads." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    fftw_plan_with_nthreads(thread_count);
    
    // init MPI
    fftw_mpi_init();
    
    //+/////////////////
    // MPI info -> rank and size
    //+/////////////////
    
    // rank
    constexpr auto invalid_rank = -1;
    auto world_rank = invalid_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    if (world_rank == invalid_rank)
    {
        std::cerr << "Fatal error: Could not get MPI comm. rank." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // size
    auto world_size = int{};
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_size == 0 || world_rank == invalid_rank)
    {
        std::cerr << "Fatal error: Could not get MPI comm. size." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    //+/////////////////
    // tests
    //+/////////////////
    
    {
        const auto n = 4;
        if (world_rank == 0)
        {
            std::cout << "Starting test with n=" << n << ".\n";
        }
        run_test(n, 1E-3, world_rank, world_size);
        if (world_rank == 0)
        {
            std::cout << "Done.\n";
        }
    }
    
    {
        const auto n = 100;
        if (world_rank == 0)
        {
            std::cout << "Starting test with n=" << n << ".\n";
        }
        run_test(n, 1E-3, world_rank, world_size);
        if (world_rank == 0)
        {
            std::cout << "Done.\n";
        }
    }
    
    {
        const auto n = 200;
        if (world_rank == 0)
        {
            std::cout << "Starting test with n=" << n << ".\n";
        }
        run_test(n, 1E-3, world_rank, world_size);
        if (world_rank == 0)
        {
            std::cout << "Done.\n";
        }
    }
    
    // report
    if (world_rank == 0)
    {
        std::cout << "All API comparison tests have been passed!" << std::endl;
    }
    
    //+/////////////////
    // clean up
    //+/////////////////
    
    // FFTW3
    fftw_cleanup_threads();
    
    // MPI
    MPI_Finalize();
    
    return 0;
}

// END OF TESTFILE
