//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'proc_plan'.

#include "implementation/proc_plan.h"

#include "gtest/gtest.h"


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// Helper functions
//+////////////////////////////////////////////////////////////////////////////////////////////////
using proc_coord_t = std::array<std::size_t, 3>;

bool proc_coord_equal(const proc_coord_t& v1, const proc_coord_t& v2)
{
    return v1[0] == v2[0] && v1[1] == v2[1] && v1[2] == v2[2];
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// Tests
//+////////////////////////////////////////////////////////////////////////////////////////////////

TEST(testsuite_proc_plan, plan1)
{
    // size constants
    const auto p = std::size_t{3};
    
    // error cases
    #ifndef NDEBUG
    ASSERT_THROW(get_proc_id(0, p, 0, 0), std::invalid_argument);
    ASSERT_THROW(get_proc_coords(p, p * p * p), std::invalid_argument);
    #endif
    
    // tests: all proc ids
    ASSERT_EQ(0, get_proc_id(p, 0, 0, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 0, 0}, get_proc_coords(p, 0)));
    ASSERT_EQ(1, get_proc_id(p, 0, 0, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 0, 1}, get_proc_coords(p, 1)));
    ASSERT_EQ(2, get_proc_id(p, 0, 0, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 0, 2}, get_proc_coords(p, 2)));
    ASSERT_EQ(3, get_proc_id(p, 0, 1, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 1, 0}, get_proc_coords(p, 3)));
    ASSERT_EQ(4, get_proc_id(p, 0, 1, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 1, 1}, get_proc_coords(p, 4)));
    ASSERT_EQ(5, get_proc_id(p, 0, 1, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 1, 2}, get_proc_coords(p, 5)));
    ASSERT_EQ(6, get_proc_id(p, 0, 2, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 2, 0}, get_proc_coords(p, 6)));
    ASSERT_EQ(7, get_proc_id(p, 0, 2, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 2, 1}, get_proc_coords(p, 7)));
    ASSERT_EQ(8, get_proc_id(p, 0, 2, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{0, 2, 2}, get_proc_coords(p, 8)));
    
    ASSERT_EQ(9, get_proc_id(p, 1, 0, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 0, 0}, get_proc_coords(p, 9)));
    ASSERT_EQ(10, get_proc_id(p, 1, 0, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 0, 1}, get_proc_coords(p, 10)));
    ASSERT_EQ(11, get_proc_id(p, 1, 0, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 0, 2}, get_proc_coords(p, 11)));
    ASSERT_EQ(12, get_proc_id(p, 1, 1, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 1, 0}, get_proc_coords(p, 12)));
    ASSERT_EQ(13, get_proc_id(p, 1, 1, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 1, 1}, get_proc_coords(p, 13)));
    ASSERT_EQ(14, get_proc_id(p, 1, 1, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 1, 2}, get_proc_coords(p, 14)));
    ASSERT_EQ(15, get_proc_id(p, 1, 2, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 2, 0}, get_proc_coords(p, 15)));
    ASSERT_EQ(16, get_proc_id(p, 1, 2, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 2, 1}, get_proc_coords(p, 16)));
    ASSERT_EQ(17, get_proc_id(p, 1, 2, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{1, 2, 2}, get_proc_coords(p, 17)));
    
    ASSERT_EQ(18, get_proc_id(p, 2, 0, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 0, 0}, get_proc_coords(p, 18)));
    ASSERT_EQ(19, get_proc_id(p, 2, 0, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 0, 1}, get_proc_coords(p, 19)));
    ASSERT_EQ(20, get_proc_id(p, 2, 0, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 0, 2}, get_proc_coords(p, 20)));
    ASSERT_EQ(21, get_proc_id(p, 2, 1, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 1, 0}, get_proc_coords(p, 21)));
    ASSERT_EQ(22, get_proc_id(p, 2, 1, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 1, 1}, get_proc_coords(p, 22)));
    ASSERT_EQ(23, get_proc_id(p, 2, 1, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 1, 2}, get_proc_coords(p, 23)));
    ASSERT_EQ(24, get_proc_id(p, 2, 2, 0));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 2, 0}, get_proc_coords(p, 24)));
    ASSERT_EQ(25, get_proc_id(p, 2, 2, 1));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 2, 1}, get_proc_coords(p, 25)));
    ASSERT_EQ(26, get_proc_id(p, 2, 2, 2));
    ASSERT_TRUE(proc_coord_equal(proc_coord_t{2, 2, 2}, get_proc_coords(p, 26)));
}

// END OF TESTFILE
