//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'schedule_thread_work'.

#include "implementation/schedule_thread_work.h"

#include "gtest/gtest.h"

#include <omp.h>

#include <vector> // std::vector
#include <iostream>


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// helper
//+////////////////////////////////////////////////////////////////////////////////////////////////

bool test_schedule_thread_work_exclude_threads(const int thread_count, const int exclude_count, const std::size_t& work_size,
    const std::vector<std::vector<std::size_t>>& solution)
{
    // preconditions
    if (exclude_count >= thread_count)
    {
        throw std::invalid_argument("test_schedule_thread_work_exclude_threads_invalid_exclude_count_error");
    }
    if (work_size == 0)
    {
        throw std::invalid_argument("test_schedule_thread_work_exclude_threads_invalid_work_size_error");
    }
    if (solution.size() != thread_count - exclude_count)
    {
        throw std::invalid_argument("test_schedule_thread_work_exclude_threads_invalid_solution_container_error");
    }
    
    // a lambda to fail the test
    auto test_passed = true;
    const auto fail_test = [&test_passed](const std::string& reason)
    {
        #pragma omp critical
        {
            test_passed = false;
            std::cout << "Test failed! " << reason << std::endl;
        }
    };
    
    // do the work parallely
    auto work_container = std::vector(work_size, false);
    #pragma omp parallel num_threads(thread_count) firstprivate(solution)
    {
        const auto thread_num = omp_get_thread_num();
        if (thread_num >= exclude_count)
        {
            // get the work-indices
            const auto work_indices = schedule_thread_work(work_size, exclude_count);
                        
            // check the results
            const auto thread_index = thread_num - exclude_count;
            if (solution[thread_index].size() != work_indices.size())
            {
                fail_test("Work size not matching for thread " + std::to_string(thread_num) + " ("
                    + std::to_string(solution[thread_index].size()) + " != " + std::to_string(work_indices.size()) + ").");
            }
            else
            {
                for (auto index = std::size_t{}; index < work_indices.size(); ++index)
                {
                    if (solution[thread_index][index] != work_indices[index])
                    {
                        fail_test("Work index not matching for thread " + std::to_string(thread_num) + " ("
                            + std::to_string(solution[thread_index][index]) + " != " + std::to_string(work_indices[index]) + ").");
                        break;
                    }
                    #pragma omp critical
                    {
                        work_container[work_indices[index]] = true;
                    }
                }
            }
        }
    }
    
    // check to see if the work was done
    for (auto index = std::size_t{}; index < work_container.size(); ++index)
    {
        if (!work_container[index])
        {
            fail_test("Work index " + std::to_string(index) + " not done.");
            break;
        }
    }
    
    return test_passed;
}

//+////////////////////////////////////////////////////////////////////////////////////////////////
// tests (exclude no threads)
//+////////////////////////////////////////////////////////////////////////////////////////////////

TEST(testsuite_static_scheduling, thread5_work9)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 0, 9, {{0, 6}, {1, 7}, {2, 8}, {3}, {4}, {5}}));
}

TEST(testsuite_static_scheduling, thread5_work10)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 0, 10, {{0, 6}, {1, 7}, {2, 8}, {3, 9}, {4}, {5}}));
}

TEST(testsuite_static_scheduling, thread5_work11)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 0, 11, {{0, 6}, {1, 7}, {2, 8}, {3, 9}, {4, 10}, {5}}));
}

TEST(testsuite_static_scheduling, thread5_work14)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 0, 14, {{0, 6, 12}, {1, 7, 13}, {2, 8}, {3, 9}, {4, 10}, {5, 11}}));
}

TEST(testsuite_static_scheduling, thread5_work15)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 0, 15, {{0, 6, 12}, {1, 7, 13}, {2, 8, 14}, {3, 9}, {4, 10}, {5, 11}}));
}

TEST(testsuite_static_scheduling, thread5_work16)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 0, 16, {{0, 6, 12}, {1, 7, 13}, {2, 8, 14}, {3, 9, 15}, {4, 10}, {5, 11}}));
}

TEST(testsuite_static_scheduling, thread1_work5)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(1, 0, 5, {{0, 1, 2, 3, 4}}));
}

//+////////////////////////////////////////////////////////////////////////////////////////////////
// tests (exclude 1 thread)
//+////////////////////////////////////////////////////////////////////////////////////////////////

TEST(testsuite_static_scheduling, thread5_work9_exclude_1thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 1, 9, {{0, 5}, {1, 6}, {2, 7}, {3, 8}, {4}}));
}

TEST(testsuite_static_scheduling, thread5_work10_exclude_1thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 1, 10, {{0, 5}, {1, 6}, {2, 7}, {3, 8}, {4, 9}}));
}

TEST(testsuite_static_scheduling, thread5_work11_exclude_1thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 1, 11, {{0, 5, 10}, {1, 6}, {2, 7}, {3, 8}, {4, 9}}));
}

TEST(testsuite_static_scheduling, thread5_work14_exclude_1thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 1, 14, {{0, 5, 10}, {1, 6, 11}, {2, 7, 12}, {3, 8, 13}, {4, 9}}));
}

TEST(testsuite_static_scheduling, thread5_work15_exclude_1thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 1, 15, {{0, 5, 10}, {1, 6, 11}, {2, 7, 12}, {3, 8, 13},
        {4, 9, 14}}));
}

TEST(testsuite_static_scheduling, thread5_work16_exclude_1thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 1, 16, {{0, 5, 10, 15}, {1, 6, 11}, {2, 7, 12}, {3, 8, 13},
        {4, 9, 14}}));
}

TEST(testsuite_static_scheduling, thread5_work17_exclude_1thread)
{
    auto test_passed = false;
    #pragma omp parallel num_threads(6)
    {
        const auto work_indices = schedule_thread_work(17, 1);
        if (omp_get_thread_num() == 0)
        {
            test_passed = work_indices.empty();
        }
    }
    ASSERT_TRUE(test_passed);
}

TEST(testsuite_static_scheduling, thread1_work5_exclude_1thread)
{
    #ifndef NDEBUG
    ASSERT_THROW(schedule_thread_work(5, 1), std::invalid_argument);
    #endif
}




//+////////////////////////////////////////////////////////////////////////////////////////////////
// tests (exclude 2 threads)
//+////////////////////////////////////////////////////////////////////////////////////////////////

TEST(testsuite_static_scheduling, thread4_work9_exclude_2thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 2, 9, {{0, 4, 8}, {1, 5}, {2, 6}, {3, 7}}));
}

TEST(testsuite_static_scheduling, thread4_work10_exclude_2thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 2, 10, {{0, 4, 8}, {1, 5, 9}, {2, 6}, {3, 7}}));
}

TEST(testsuite_static_scheduling, thread4_work11_exclude_2thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 2, 11, {{0, 4, 8}, {1, 5, 9}, {2, 6, 10}, {3, 7}}));
}

TEST(testsuite_static_scheduling, thread4_work14_exclude_2thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 2, 14, {{0, 4, 8, 12}, {1, 5, 9, 13}, {2, 6, 10}, {3, 7, 11}}));
}

TEST(testsuite_static_scheduling, thread4_work15_exclude_2thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 2, 15, {{0, 4, 8, 12}, {1, 5, 9, 13}, {2, 6, 10, 14}, {3, 7, 11}}));
}

TEST(testsuite_static_scheduling, thread4_work16_exclude_2thread)
{
    ASSERT_TRUE(test_schedule_thread_work_exclude_threads(6, 2, 16, {{0, 4, 8, 12}, {1, 5, 9, 13}, {2, 6, 10, 14},
        {3, 7, 11, 15}}));
}

TEST(testsuite_static_scheduling, thread4_work17_exclude_2thread)
{
    auto test_passed1 = false;
    auto test_passed2 = false;
    #pragma omp parallel num_threads(6)
    {
        const auto work_indices = schedule_thread_work(17, 2);
        if (omp_get_thread_num() == 0)
        {
            test_passed1 = work_indices.empty();
        }
        if (omp_get_thread_num() == 1)
        {
            test_passed2 = work_indices.empty();
        }
    }
    ASSERT_TRUE(test_passed1 && test_passed2);
}

TEST(testsuite_static_scheduling, thread1_work5_exclude_2thread)
{
    #ifndef NDEBUG
    #pragma omp parallel num_threads(2)
    {
        ASSERT_THROW(schedule_thread_work(5, 2), std::invalid_argument);
    }
    #endif
}

// END OF TESTFILE
