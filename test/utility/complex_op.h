//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_COMPLEX_OP_H
#define S3DFT_COMPLEX_OP_H

/// \file This file contains additional operations on 'complex'.

#include "implementation/complex.h"

#include <ostream> // std::ostream
#include <cmath> // std::fabs


namespace s3dft
{
namespace test
{

//+//////////////////////////////////////////////
// Arithmetic
//+//////////////////////////////////////////////

/// function for complex number scaling (scalar is left-operand)
template<typename Treal_t>
complex<Treal_t> operator*(const Treal_t& c1, const complex<Treal_t>& c2)
{
    auto result = complex<Treal_t>{};
    result.real = c1 * c2.real;
    result.imag = c1 * c2.imag;
    return result;
}


/// function for complex number scaling (scalar is right-operand)
template<typename Treal_t>
complex<Treal_t> operator*(const complex<Treal_t>& c1, const Treal_t& c2)
{
    return c2 * c1;
}


/// function for complex number multiplication
template<typename Treal_t>
complex<Treal_t> operator*(const complex<Treal_t>& c1, const complex<Treal_t>& c2)
{
    auto result = complex<Treal_t>();
    result.real = c1.real * c2.real - c1.imag * c2.imag;
    result.imag = c1.real * c2.imag + c1.imag * c2.real;
    return result;
}


/// function for complex number addition
template<typename Treal_t>
complex<Treal_t> operator+(const complex<Treal_t>& c1, const complex<Treal_t>& c2)
{
    auto result = complex<Treal_t>{};
    result.real = c1.real + c2.real;
    result.imag = c1.imag + c2.imag;
    return result;
}


//+//////////////////////////////////////////////
// Comparison
//+//////////////////////////////////////////////

/// function for complex number comparison (by equality)
template<typename Treal_t>
bool equals(const complex<Treal_t>& c1, const complex<Treal_t>& c2, const Treal_t& tol = 1E-16)
{
    return std::fabs(c1.real - c2.real) < tol && std::fabs(c1.imag - c2.imag) < tol;
}

} // namespace test
} // namespace s3dft
#endif
