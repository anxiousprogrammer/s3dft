//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'shared_mem_3d_dft'.

#include "implementation/schedule_thread_work.h"
#include "implementation/tensor_matrix_math.h"
#include "implementation/transpose.h"
#include "shared_mem_3d_dft.h"

#include "mkl.h"

#include <omp.h>

#include <cmath> // std::atan, std::cos, std::sin
#include <stdexcept> // std::runtime_error


namespace s3dft::test
{

//+//////////////////////////////////////////////
// Implementation
//+//////////////////////////////////////////////

namespace implementation
{

template<class Treal_t>
matrix<Treal_t> get_coeff_matrix(const std::size_t& n)
{
    // pre-conditions
    #ifndef NDEBUG
    if (n < 2)
    {
        throw std::invalid_argument("get_coeff_matrix_invalid_size_error");
    }
    #endif
    
    // constants
    const auto pi_t_2 = std::atan(static_cast<Treal_t>(1.0)) * static_cast<Treal_t>(8.0);
    const auto n_inv = static_cast<Treal_t>(1.0) / static_cast<Treal_t>(n);
    const auto k = pi_t_2 * n_inv;
    
    // calculate the coefficients
    auto result = matrix<Treal_t>(n);
    #pragma omp parallel for
    for (auto n_index = std::size_t{}; n_index < n; ++n_index)
    {
        for (auto k_index = n_index; k_index < n; ++k_index)
        {
            const auto param = k * static_cast<Treal_t>(n_index) * static_cast<Treal_t>(k_index);
            result(n_index, k_index) = {static_cast<Treal_t>(std::cos(param)), -static_cast<Treal_t>(std::sin(param))};
        }
    }
    for (auto n_index = std::size_t{}; n_index < n; ++n_index)
    {
        for (auto k_index = std::size_t{}; k_index < n_index; ++k_index)
        {
            result(n_index, k_index) = result(k_index, n_index);
        }
    }
    
    return result;
}

} // namespace implementation


template<class Treal_t>
matrix<Treal_t> create_coeff_matrix(const std::size_t& n)
{
    #ifndef NDEBUG
    if (n < 1)
    {
        throw std::invalid_argument("create_coeff_matrix_invalid_size_error");
    }
    #endif
    
    // calculate the coefficient matrix
    return implementation::get_coeff_matrix<Treal_t>(n);
}


template<class Treal_t>
scratch_cache<Treal_t> create_scratch_cache(const std::size_t& n)
{
    #ifndef NDEBUG
    if (n < 1)
    {
        throw std::invalid_argument("create_scratch_tensor_invalid_size_error");
    }
    #endif
    
    auto scache = scratch_cache<Treal_t>{tensor<Treal_t>(n), tensor<Treal_t>(n)};
    
    #pragma omp parallel
    {
        const auto work_indices = schedule_thread_work(scache.tens0.get_n(), 0);
        tensor_zero(scache.tens0, work_indices);
        tensor_zero(scache.tens1, work_indices);
    }
    
    return scache;
}


template<class Treal_t>
void execute(tensor<Treal_t>& data, const matrix<Treal_t>& coeff, scratch_cache<Treal_t>& scache)
{
    // renaming for convenience
    auto& tens0 = data; // final result lands here
    auto& tens1 = scache.tens0;
    auto& tens2 = scache.tens1;
    
    // open up the parallel region
    #pragma omp parallel
    {
        // stage 1:
        const auto work_indices = schedule_thread_work(tens0.get_n(), 0);
        tensor_zero(tens1, work_indices);
        ::s3dft::shared_mem::tensor_matrix_mult(tens0, coeff, tens1, work_indices);
        
        // stage 2:
        transpose23(tens1, tens0, work_indices);
        tensor_zero(tens1, work_indices);
        ::s3dft::shared_mem::tensor_matrix_mult(tens0, coeff, tens1, work_indices);
        transpose23(tens1, tens2, work_indices);
        
        // prep for stage 3: transposition of intermediate data
        #pragma omp barrier
        transpose13(tens2, tens0);
        
        // stage 3:
        tensor_zero(tens1, work_indices);
        ::s3dft::shared_mem::tensor_matrix_mult(tens0, coeff, tens1, work_indices);
        
        // finally, result data is transposed back into 'original' layout
        #pragma omp barrier
        transpose13(tens1, tens0);
    }
}



//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template matrix<float> create_coeff_matrix<float>(const std::size_t&);
template matrix<double> create_coeff_matrix<double>(const std::size_t&);

template scratch_cache<float> create_scratch_cache<float>(const std::size_t&);
template scratch_cache<double> create_scratch_cache<double>(const std::size_t&);

template void execute<float>(tensor<float>&, const matrix<float>&, scratch_cache<float>&);
template void execute<double>(tensor<double>&, const matrix<double>&, scratch_cache<double>&);

} // namespace s3dft::test

// END OF IMPLEMENTATION
