//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_STREAM_FUNCTIONS_H
#define S3DFT_STREAM_FUNCTIONS_H

/// \file This file contains functions which can be used to output certain types to an output stream

#include "implementation/complex.h"
#include "matrix.h"
#include "tensor.h"

#include <ostream> // std::ostream
#include <cmath> // std::fabs
#include <fstream> // std::ofstream
#include <limits> // std::numeric_limits
#include <iomanip> // std::setprecision


namespace s3dft
{
namespace test
{

//+//////////////////////////////////////////////
// Complex
//+//////////////////////////////////////////////

template<typename Treal_t>
std::ostream& operator<<(std::ostream& ostr, const complex<Treal_t>& op)
{
    ostr << "(" << op.real << ", " << op.imag << ")";
    return ostr;
}


//+//////////////////////////////////////////////
// Matrix
//+//////////////////////////////////////////////

template<typename Treal_t>
std::ostream& operator<<(std::ostream& ostr, const matrix<Treal_t>& op)
{
    for (auto i1 = std::size_t{}; i1 < op.get_n1(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < op.get_n2(); ++i2)
        {
            ostr << "(" << op(i1, i2).real << ", " << op(i1, i2).imag << ")   ";
        }
        ostr << "\n";
    }
    return ostr;
}


//+//////////////////////////////////////////////
// Tensor
//+//////////////////////////////////////////////

template<typename Treal_t>
std::ostream& operator<<(std::ostream& ostr, const tensor<Treal_t>& op)
{
    for (auto i1 = std::size_t{}; i1 < op.get_n1(); ++i1)
    {
        ostr << "Slice " << i1 << "\n";
        for (auto i2 = std::size_t{}; i2 < op.get_n2(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < op.get_n3(); ++i3)
            {
                ostr << "(" << op(i1, i2, i3).real << ", " << op(i1, i2, i3).imag << ")   ";
            }
            ostr << "\n";
        }
    }
    return ostr;
}


//+//////////////////////////////////////////////
// File dumper
//+//////////////////////////////////////////////

template<class Ttype_t>
void dump_to_file(const std::string& filename, const Ttype_t& data)
{
    // open the file
    auto writer = std::ofstream(filename, std::ios_base::trunc);
    if (!writer.is_open())
    {
        throw std::runtime_error("dump_to_file_could_not_open_file");
    }
    
    // write the contents
    writer << std::fixed << std::setprecision(15) << data;
    
    // close the file
    writer.close();
    if (writer.is_open())
    {
        throw std::runtime_error("dump_to_file_could_not_close_file");
    }
}

} // namespace test
} // namespace s3dft
#endif
