#!/bin/bash
printf "\n---------------------------------------\nTEST: transpose\n"
./test_transpose

printf "\n---------------------------------------\nTEST: tensor\n"
./test_tensor

printf "\n---------------------------------------\nTEST: shared_mem_tensor_matrix_math\n"
./test_shared_mem_tensor_matrix_math

printf "\n---------------------------------------\nTEST: shared_mem_3d_dft\n"
./test_shared_mem_3d_dft

printf "\n---------------------------------------\nTEST: shared_mem_3d_dft_fftw3\n"
./test_shared_mem_3d_dft_fftw3

printf "\n---------------------------------------\nTEST: schedule_thread_work\n"
./test_schedule_thread_work

printf "\n---------------------------------------\nTEST: proc_plan\n"
./test_proc_plan

printf "\n---------------------------------------\nTEST: matrix\n"
./test_matrix

printf "\n---------------------------------------\nTEST: distr_mem_tensor_matrix_math\n"
mpirun -n 27 ./test_distr_mem_tensor_matrix_math

printf "\n---------------------------------------\nTEST: api\n"
mpirun -n 27 ./test_api

printf "\n---------------------------------------\nTEST: api compare\n"
mpirun -n 8 ./test_api_compare
