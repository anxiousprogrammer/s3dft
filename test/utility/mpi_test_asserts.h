//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef S3DFT_MPI_TEST_ASSERTS_H
#define S3DFT_MPI_TEST_ASSERTS_H

/// \file This file contains simple test-macros for MPI-based programs.

#include "implementation/complex.h"

#include <mpi.h>

#include <iostream> // std::cerr
#include <type_traits> // std::is_same, std::remove_cv_t, std::remove_reference_t
#include <cstdlib> // std::fabs
#include <iomanip> // std::setprecision, std::fixed


#define mpi_assert_true(cond) \
{ \
    if (!cond) \
    { \
        std::cout << "Test failed! line: " << __LINE__ << std::endl; \
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER); \
    } \
}

#define mpi_assert_equal(V1, V2) \
{ \
    static_assert(std::is_same<decltype(V1), decltype(V2)>::value);\
    if (V1 != V2) \
    { \
        std::cout << "Test failed! Line: " << __LINE__ << "; Reason: " << V1 << " != " << V2 << std::endl; \
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER); \
    } \
}

#define mpi_assert_fp_equal(V1, V2, tol) \
{ \
    static_assert(std::is_same<float, std::remove_cv_t<std::remove_reference_t<decltype(V1)>>>::value \
        || std::is_same<double, std::remove_cv_t<std::remove_reference_t<decltype(V1)>>>::value);\
    static_assert(std::is_same<float, std::remove_cv_t<std::remove_reference_t<decltype(V2)>>>::value \
        || std::is_same<double, std::remove_cv_t<std::remove_reference_t<decltype(V2)>>>::value);\
    static_assert(std::is_same<float, std::remove_cv_t<std::remove_reference_t<decltype(tol)>>>::value \
        || std::is_same<double, std::remove_cv_t<std::remove_reference_t<decltype(tol)>>>::value);\
    if (std::fabs(V1 - V2) > std::fabs(tol)) \
    { \
        std::cout << "Test failed! Line: " << __LINE__ << "; Reason: " << std::fixed << std::setprecision(13) << V1 << " != " << V2 \
            << std::endl; \
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER); \
    } \
}

#endif
