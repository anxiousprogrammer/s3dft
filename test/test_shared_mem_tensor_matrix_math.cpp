//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains tests for tensor/matrix math operations.

#include "implementation/schedule_thread_work.h"
#include "implementation/tensor_matrix_math.h"

#include "utility/complex_op.h"
#include "utility/core_count.h"

#include "gtest/gtest.h"


using namespace s3dft;
using namespace s3dft::test;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// helper functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
void step_fill(tensor<Treal_t>& tens, const std::size_t& start_step)
{
    auto value = start_step;
    for (auto i1 = std::size_t{}; i1 < tens.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < tens.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < tens.get_n(); ++i3, ++value)
            {
                tens(i1, i2, i3) = {static_cast<Treal_t>(value++), static_cast<Treal_t>(value)};
            }
        }
    }
}

template<class Treal_t>
void step_fill(matrix<Treal_t>& mat, const std::size_t& start_step)
{
    auto value = start_step;
    for (auto index = std::size_t{}; index < mat.size(); ++index, ++value)
    {
        mat.data()[index] = {static_cast<Treal_t>(2 * value++), static_cast<Treal_t>(2 * value)};
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// tests: tensor_matrix_mult
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
void test_tensor_matrix_mult()
{
    // input operands
    auto tens = tensor<Treal_t>(2);
    step_fill(tens, 1);
    // ^ 2x3x2:
    //
    // 1.0 + 2.0i     3.0 + 4.0i
    // 5.0 + 6.0i     7.0 + 8.0i
    //
    // 9.0 + 10.0i    11.0 + 12.0i
    // 13.0 + 14.0i   15.0 + 16.0i
    
    auto mat = matrix<Treal_t>(2);
    step_fill(mat, 3);
    // ^ 3x3:
    //
    // 6.0 + 8.0i     10.0 + 12.0i
    // 14.0 + 16.0i   18.0 + 20.0i
    
    auto result = tensor<Treal_t>(2);
    
    // error case(s)
    #ifndef NDEBUG
    {
        auto error_result = tensor<Treal_t>(3);
        const auto work_indices = schedule_thread_work(tens.get_n(), 1);
        ASSERT_THROW(shared_mem::tensor_matrix_mult(tens, mat, error_result, work_indices), std::invalid_argument);
    }
    {
        auto error_mat = mat<Treal_t>(3);
        const auto work_indices = schedule_thread_work(tens.get_n(), 1);
        ASSERT_THROW(shared_mem::tensor_matrix_mult(tens, error_mat, result, work_indices), std::invalid_argument);
    }
    #endif
    
    // perform the multiplication operation
    
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(tens.get_n(), 1);
        shared_mem::tensor_matrix_mult(tens, mat, result, work_indices);
    }
    
    // validate the results
    ASSERT_TRUE(equals(result(0, 0, 0), {static_cast<Treal_t>(-32.0), static_cast<Treal_t>(124.0)}));
    ASSERT_TRUE(equals(result(0, 0, 1), {static_cast<Treal_t>(-40.0), static_cast<Treal_t>(164.0)}));
    ASSERT_TRUE(equals(result(0, 1, 0), {static_cast<Treal_t>(-48.0), static_cast<Treal_t>(300.0)}));
    ASSERT_TRUE(equals(result(0, 1, 1), {static_cast<Treal_t>(-56.0), static_cast<Treal_t>(404.0)}));
    
    ASSERT_TRUE(equals(result(1, 0, 0), {static_cast<Treal_t>(-64.0), static_cast<Treal_t>(476.0)}));
    ASSERT_TRUE(equals(result(1, 0, 1), {static_cast<Treal_t>(-72.0), static_cast<Treal_t>(644.0)}));
    ASSERT_TRUE(equals(result(1, 1, 0), {static_cast<Treal_t>(-80.0), static_cast<Treal_t>(652.0)}));
    ASSERT_TRUE(equals(result(1, 1, 1), {static_cast<Treal_t>(-88.0), static_cast<Treal_t>(884.0)}));
}

TEST(testsuite_blas_integration, tensor_matrix_mult_sp)
{
    test_tensor_matrix_mult<float>();
}

TEST(testsuite_blas_integration, tensor_matrix_mult_dp)
{
    test_tensor_matrix_mult<double>();
}

// END OF TESTFILE
