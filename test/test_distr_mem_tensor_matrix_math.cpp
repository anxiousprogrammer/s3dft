//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the tests for S3DFT's distributed memory tensor-matrix multiplication operations.

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "implementation/tensor_matrix_math.h"
#include "implementation/transpose.h"

#include "utility/mpi_test_asserts.h"
#include "utility/core_count.h"

#include "mkl.h"

#include <omp.h>
#include <mpi.h>

#include <cmath> // std::cos, std::sin, std::atan
#include <cstdlib> // std::rand
#include <stdexcept> // std::runtime_error
#include <algorithm> // std::fill


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// helper functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

tensor<double> get_random_tensor(const std::size_t& n)
{
    auto result = tensor<double>(n);
    for (auto index = std::size_t{}; index < result.size(); ++index)
    {
        const auto rand_value = []()
        {
            return std::sin(static_cast<double>(std::rand() % 10 + 1) / static_cast<double>(std::rand() % 10 + 1));
        };
        result.data()[index] = {rand_value(), rand_value()};
    }
    
    return result;
}

matrix<double> get_random_matrix(const std::size_t& n)
{
    auto result = matrix<double>(n);
    for (auto index = std::size_t{}; index < result.size(); ++index)
    {
        const auto rand_value = []()
        {
            return std::sin(static_cast<double>(std::rand() % 10 + 1) / static_cast<double>(std::rand() % 10 + 1));
        };
        result.data()[index] = {rand_value(), rand_value()};
    }
    
    return result;
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// test function
//+////////////////////////////////////////////////////////////////////////////////////////////////

void run_test(int world_size, int world_rank, const std::size_t& n, const std::size_t& p, const double& tolerance)
{
    // compute block size
    const auto b = n / p;
    if (std::fabs(static_cast<double>(b) - static_cast<double>(n) / static_cast<double>(p)) > 1E-12)
    {
        std::cerr << "Fatal error: n cannot be divided by p." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    auto tens = get_random_tensor(n);
    auto mat = get_random_matrix(n);
    
    //+//////////////////////////////////////////
    // distribute the data
    //+//////////////////////////////////////////
    
    auto tens_block = tensor<double>(b);
    auto mat_block = matrix<double>(b);
    if (world_rank == 0)
    {
        // perform domain decomposition of tens_block_domain
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    // copy the data of the block domain into a container
                    auto tens_block_sendbuf = tensor<double>(b);
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                tens_block_sendbuf(i1, i2, i3) = tens(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                            }
                        }
                    }
                    
                    // in case of the root process
                    const auto target_proc_id = get_proc_id(p, bi1, bi2, bi3);
                    if (target_proc_id == 0)
                    {
                        std::copy(tens_block_sendbuf.data(), tens_block_sendbuf.data() + tens_block_sendbuf.size(),
                            tens_block.data());
                        continue;
                    }
                    
                    // send the tensor's block domain to its associated process
                    MPI_Send(tens_block_sendbuf.data(), 2 * tens_block_sendbuf.size(), MPI_DOUBLE, target_proc_id, 0,
                        MPI_COMM_WORLD);
                }
            }
        }
        
        // perform domain decomposition of mat_block_domain
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {   
                // copy the data of the block domain into a container
                auto mat_block_sendbuf = matrix<double>(b);
                for (auto i1 = std::size_t{}; i1 < b; ++i1)
                {
                    for (auto i2 = std::size_t{}; i2 < b; ++i2)
                    {
                        mat_block_sendbuf(i1, i2) = mat(bi1 * b + i1, bi2 * b + i2);
                    }
                }
                
                // distribute along the slices
                for (auto slice_index = std::size_t{}; slice_index < p; ++slice_index)
                {
                    // in case of the root process...
                    const auto target_proc_id = get_proc_id(p, slice_index, bi1, bi2);
                    if (target_proc_id == 0)
                    {
                        std::copy(mat_block_sendbuf.data(), mat_block_sendbuf.data() + mat_block_sendbuf.size(),
                            mat_block.data());
                        continue;
                    }
                    
                    // send the matrix's block domain to its associated process
                    MPI_Send(mat_block_sendbuf.data(), 2 * mat_block_sendbuf.size(), MPI_DOUBLE, target_proc_id, 1, MPI_COMM_WORLD);
                }
            }
        }
    }
    else // non-root processes
    {
        // start obtaining the tensor-block
        auto req1 = MPI_Request();
        MPI_Irecv(tens_block.data(), 2 * tens_block.size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &req1);
        
        // start obtaining the matrix-block
        auto req2 = MPI_Request();
        MPI_Irecv(mat_block.data(), 2 * mat_block.size(), MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &req2);
        
        // finish up with comm.
        const auto wait1_code = MPI_Wait(&req1, MPI_STATUS_IGNORE);
        const auto wait2_code = MPI_Wait(&req2, MPI_STATUS_IGNORE);
        if (wait1_code != MPI_SUCCESS || wait2_code != MPI_SUCCESS)
        {
            throw std::runtime_error("domain_decomposition_irecv_wait_error");
        }
    }
    
    //+//////////////////////////////////////////
    // test of 'tensor_matrix_mult'
    //+//////////////////////////////////////////
    
    // data
    auto result_block = tensor<double>(b);
    auto scratch_tens = tensor<double>(b);
    auto scratch_mat = matrix<double>(b);
    
    // perform the multiplication
    auto tens_block_copy = tensor<double>(b);
    auto mat_block_copy = matrix<double>(b);
    #pragma omp parallel
    {
        const auto work_indices = schedule_thread_work(result_block.get_n(), 1);
        if (omp_get_thread_num() == COMM_THREAD)
        {
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), mat_block_copy.data());
        }
        else
        {
            tensor_zero(result_block, work_indices);
            tensor_copy(tens_block, tens_block_copy, work_indices);
        }
        #pragma omp barrier
        distr_mem::tensor_matrix_mult(p, MPI_COMM_WORLD, world_rank, tens_block_copy, mat_block_copy, result_block, scratch_tens,
            scratch_mat, work_indices);
    }
    
    if (world_rank == 0)
    {
        // gather back the data
        auto received_tensors = std::vector(world_size, tensor<double>(b));
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(result_block.get_n(), 0);
            tensor_copy(result_block, received_tensors[0], work_indices);
        }
        auto requests = std::vector(world_size - 1, MPI_Request());
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            MPI_Irecv(received_tensors[index].data(), 2 * received_tensors[index].size(), MPI_DOUBLE, index, 0, MPI_COMM_WORLD,
                &requests[index - 1]);
        }
        MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE);
        
        // calculate the solution
        auto solution = tensor<double>(n);
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(solution.get_n(), 1);
            tensor_zero(solution, work_indices);
            shared_mem::tensor_matrix_mult(tens, mat, solution, work_indices);
        }
        
        // compare the obtained results against the solution
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    const auto& relevant_result_block = received_tensors[get_proc_id(p, bi1, bi2, bi3)];
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                const auto& v1 = solution(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                                const auto& v2 = relevant_result_block(i1, i2, i3);
                                mpi_assert_fp_equal(v1.real, v2.real, 1E-12);
                                mpi_assert_fp_equal(v1.imag, v2.imag, 1E-12);
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        MPI_Send(result_block.data(), 2 * result_block.size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    
    if (world_rank == 0)
    {
        std::cout << "The test of \'distr_mem::tensor_matrix_mult\' has been passed!" << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    
    
    //+//////////////////////////////////////////
    // test of 'transp_matrix_tensor_mult'
    //+//////////////////////////////////////////
    
    // perform the multiplication
    #pragma omp parallel
    {
        const auto work_indices = schedule_thread_work(result_block.get_n(), 1);
        if (omp_get_thread_num() == COMM_THREAD)
        {
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), mat_block_copy.data());
        }
        else
        {
            tensor_zero(result_block, work_indices);
            tensor_copy(tens_block, tens_block_copy, work_indices);
        }
        #pragma omp barrier
        distr_mem::transp_matrix_tensor_mult(p, MPI_COMM_WORLD, world_rank, mat_block_copy, tens_block_copy, result_block,
            scratch_tens, scratch_mat, work_indices);
    }
    
    if (world_rank == 0)
    {
        // gather back the data
        auto received_tensors = std::vector(world_size, tensor<double>(b));
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(result_block.get_n(), 1);
            tensor_copy(result_block, received_tensors[0], work_indices);
        }
        auto requests = std::vector(world_size - 1, MPI_Request());
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            MPI_Irecv(received_tensors[index].data(), 2 * received_tensors[index].size(), MPI_DOUBLE, index, 0, MPI_COMM_WORLD,
                &requests[index - 1]);
        }
        MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE);
        
        // calculate the solution
        auto solution = tensor<double>(n);
        auto intermediate = tensor<double>(n);
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens.get_n(), 1);
            transpose23(tens, intermediate, work_indices);
            tensor_zero(solution, work_indices);
            shared_mem::tensor_matrix_mult(intermediate, mat, solution, work_indices); // (X^T A)^T = A^T X
            transpose23(solution, intermediate, work_indices);
            tensor_copy(intermediate, solution, work_indices);
        }
        
        // compare the obtained results against the solution
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    const auto& relevant_result_block = received_tensors[get_proc_id(p, bi1, bi2, bi3)];
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                const auto& v1 = solution(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                                const auto& v2 = relevant_result_block(i1, i2, i3);
                                mpi_assert_fp_equal(v1.real, v2.real, 1E-12);
                                mpi_assert_fp_equal(v1.imag, v2.imag, 1E-12);
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        MPI_Send(result_block.data(), 2 * result_block.size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    if (world_rank == 0)
    {
        std::cout << "The test of \'distr_mem::transp_matrix_tensor_mult\' has been passed!" << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    
    
    //+//////////////////////////////////////////
    // test of 'transp_tensor_matrix_mult'
    //+//////////////////////////////////////////
    
    // perform the multiplication
    #pragma omp parallel
    {
        const auto work_indices = schedule_thread_work(result_block.get_n(), 1);
        if (omp_get_thread_num() == COMM_THREAD)
        {
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), mat_block_copy.data());
        }
        else
        {
            tensor_zero(result_block, work_indices);
        }
        #pragma omp barrier
        distr_mem::transp_tensor_matrix_mult(p, MPI_COMM_WORLD, world_rank, tens_block, mat_block, result_block, scratch_tens,
            scratch_mat, work_indices);
    }
    
    if (world_rank == 0)
    {
        // gather back the data
        auto received_tensors = std::vector(world_size, tensor<double>(b));
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(result_block.get_n(), 0);
            tensor_copy(result_block, received_tensors[0], work_indices);
        }
        auto requests = std::vector(world_size - 1, MPI_Request());
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            MPI_Irecv(received_tensors[index].data(), 2 * received_tensors[index].size(), MPI_DOUBLE, index, 0, MPI_COMM_WORLD,
                &requests[index - 1]);
        }
        MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE);
        
        // calculate the solution
        auto solution = tensor<double>(n);
        auto intermediate = tensor<double>(n);
        #pragma omp parallel
        {
            transpose13(tens, intermediate);
            const auto work_indices = schedule_thread_work(solution.get_n(), 1);
            tensor_zero(solution, work_indices);
            shared_mem::tensor_matrix_mult(intermediate, mat, solution, work_indices);
            #pragma omp barrier
            transpose13(solution, intermediate);
            #pragma omp barrier
            tensor_copy(intermediate, solution, work_indices);
        }
        
        // compare the obtained results against the solution
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    const auto& relevant_result_block = received_tensors[get_proc_id(p, bi1, bi2, bi3)];
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                const auto& v1 = solution(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                                const auto& v2 = relevant_result_block(i1, i2, i3);
                                mpi_assert_fp_equal(v1.real, v2.real, 1E-12);
                                mpi_assert_fp_equal(v1.imag, v2.imag, 1E-12);
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        MPI_Send(result_block.data(), 2 * result_block.size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    
    if (world_rank == 0)
    {
        std::cout << "The test of \'distr_mem::transp_tensor_matrix_mult\' has been passed!" << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main function
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // set the number of OpenMP and MKL threads
    //+//////////////////////////////////////////
    
    omp_set_num_threads(test::g_core_count);
    
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////
    
    int provided = 0;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "The MPI implementation does not support multi-threaded message passing" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    const auto p = std::size_t{3};
    if (world_rank == 0)
    {
        const auto p_cube = p * p * p;
        if (world_size != p_cube)
        {
            std::cerr << "Please run the test using " << p_cube << " processes" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
        }
    }
    
    //+//////////////////////////////////////////
    // print report
    //+//////////////////////////////////////////
    
    if (world_rank == 0)
    {
        std::cout << "Starting distr. mem. tensor-matrix multiplication tests with " << omp_get_max_threads() << " threads.\n"
            << std::endl;
    }
    
    //+/////////////////
    try
    {
    //+/////////////////
    
    //+//////////////////////////////////////////
    // run test for small size
    //+//////////////////////////////////////////
    
    {
        const auto n = std::size_t{45};
        if (world_rank == 0)
        {
            const auto b = n / p;
            std::cout << "Starting the small-size test (message size ~"
                << static_cast<std::size_t>(b * b * b * sizeof(complex<double>) * 1E-6) << " MB)." << std::endl;
        }
        run_test(world_size, world_rank, n, p, 1E-10);
        if (world_rank == 0)
        {
            std::cout << "Passed the small-size test.\n" << std::endl;
        }
    }
    
    //+//////////////////////////////////////////
    // run test for large size
    //+//////////////////////////////////////////
    
    {
        const auto n = std::size_t{360};
        if (world_rank == 0)
        {
            const auto b = n / p;
            std::cout << "Starting the large-size test (message size ~"
                << static_cast<std::size_t>(b * b * b * sizeof(complex<double>) * 1E-6) << " MB)" << std::endl;
        }
        run_test(world_size, world_rank, n, p, 1E-10);
        if (world_rank == 0)
        {
            std::cout << "Passed the large-size test.\n" << std::endl;
        }
    }
    
    //+/////////////////
    } catch(const std::exception& exc)
    {
        std::cerr << "An exception was caught on process " << world_rank << "; what: " << exc.what() << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    //+/////////////////
    
    MPI_Finalize();
    return 0;
}

// END OF TESTFILE
