//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the tests for S3DFT's API function.

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "distr_mem_3d_dft.h"

#include "utility/shared_mem_3d_dft.h"
#include "utility/mpi_test_asserts.h"
#include "utility/core_count.h"

#include <omp.h>
#include <mpi.h>

#include <cstdlib> // std::rand
#include <cmath> // std::sin
#include <vector> // std::vector


using namespace s3dft;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// helper functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

tensor<double> get_random_data(const std::size_t& n)
{
    auto result = tensor<double>(n);
    for (auto index = std::size_t{}; index < result.size(); ++index)
    {
        const auto rand_value = []()
        {
            return (std::rand() % 2 == 0 ? 1.0 : -1.0) * std::sin(static_cast<double>(std::rand() % 10 + 1));
        };
        result.data()[index] = {rand_value(), rand_value()};
    }
    
    return result;
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// tester functions
//+////////////////////////////////////////////////////////////////////////////////////////////////

void run_test(const std::size_t& n, const double& tolerance)
{
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    // compute block size
    const auto b = compute_block_size(n, MPI_COMM_WORLD);
    const auto p = n / b;    // at this point, we already know too much about the domain decomposition ;)
    if (n % b != 0)
    {
        std::cerr << "Fatal error: n cannot be divided by b." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    auto tens = get_random_data(n);
    
    //+//////////////////////////////////////////
    // distribute the data
    //+//////////////////////////////////////////
    
    auto tens_block = tensor<double>(b);
    if (world_rank == 0)
    {
        // perform domain decomposition of tens_block_domain
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    // copy the data of the block domain into a container
                    auto tens_block_sendbuf = tensor<double>(b);
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                tens_block_sendbuf(i1, i2, i3) = tens(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                            }
                        }
                    }
                    
                    // in case of the root process
                    const auto target_proc_id = get_proc_id(p, bi1, bi2, bi3);
                    if (target_proc_id == 0)
                    {
                        std::copy(tens_block_sendbuf.data(), tens_block_sendbuf.data() + tens_block_sendbuf.size(),
                            tens_block.data());
                        continue;
                    }
                    
                    // send the tensor's block domain to its associated process
                    MPI_Send(tens_block_sendbuf.data(), 2 * tens_block_sendbuf.size(), MPI_DOUBLE, target_proc_id, 0,
                        MPI_COMM_WORLD);
                }
            }
        }
    }
    else
    {
        // start obtaining m1's block domain
        MPI_Recv(tens_block.data(), 2 * tens_block.size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    
    //+//////////////////////////////////////////
    // 3D-DFT by BTMM
    //+//////////////////////////////////////////
    
    const auto coeff_block = create_coeff_block_matrix<double>(n, b, MPI_COMM_WORLD);
    auto scache = create_scratch_cache<double>(b);
    execute(n, tens_block, coeff_block, scache, MPI_COMM_WORLD);
    
    //+//////////////////////////////////////////
    // Comparison of results
    //+//////////////////////////////////////////
    
    // comparison of result with the solution data
    if (world_rank == 0)
    {
        // gather back the data
        auto received_tensors = std::vector(world_size, tensor<double>(b));
        #pragma omp parallel
        {
            const auto work_indices = schedule_thread_work(tens_block.get_n(), 1);
            tensor_copy(tens_block, received_tensors[0], work_indices);
        }
        auto requests = std::vector(world_size - 1, MPI_Request());
        for (auto index = std::size_t{1}; index < world_size; ++index)
        {
            MPI_Irecv(received_tensors[index].data(), 2 * received_tensors[index].size(), MPI_DOUBLE, index, 0, MPI_COMM_WORLD,
                &requests[index - 1]);
        }
        MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE);
        
        // calculate the solution
        const auto coeff_sm = test::create_coeff_matrix<double>(n);
        auto scache_sm = test::create_scratch_cache<double>(n);
        test::execute(tens, coeff_sm, scache_sm);
        const auto& solution = tens;
        
        // compare the obtained results against the solution
        //constexpr auto tolerance = double(1E-10);
        for (auto bi1 = std::size_t{}; bi1 < p; ++bi1)
        {
            for (auto bi2 = std::size_t{}; bi2 < p; ++bi2)
            {
                for (auto bi3 = std::size_t{}; bi3 < p; ++bi3)
                {
                    const auto relevant_result_block = received_tensors[get_proc_id(p, bi1, bi2, bi3)];
                    for (auto i1 = std::size_t{}; i1 < b; ++i1)
                    {
                        for (auto i2 = std::size_t{}; i2 < b; ++i2)
                        {
                            for (auto i3 = std::size_t{}; i3 < b; ++i3)
                            {
                                const auto& v1 = solution(bi1 * b + i1, bi2 * b + i2, bi3 * b + i3);
                                const auto& v2 = relevant_result_block(i1, i2, i3);
                                mpi_assert_fp_equal(v1.real, v2.real, tolerance);
                                mpi_assert_fp_equal(v1.imag, v2.imag, tolerance);
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        MPI_Send(tens_block.data(), 2 * tens_block.size(), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main function
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // set the number of OpenMP threads
    //+//////////////////////////////////////////
    
    omp_set_num_threads(test::g_core_count);
    
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////

    int provided = 0;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "The MPI implementation does not support multi-threaded message passing" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    if (world_rank == 0)
    {
        const auto p = std::size_t{3};
        const auto p_cube = p * p * p;
        if (world_size != p_cube)
        {
            std::cerr << "Please run the test using " << p_cube << " processes" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
        }
    }
    
    //+//////////////////////////////////////////
    // print report
    //+//////////////////////////////////////////
    
    if (world_rank == 0)
    {
        std::cout << "Starting distr. mem. 3D-DFT tests with " << omp_get_max_threads() << " threads.\n" << std::endl;
    }
    
    //+/////////////////
    try
    {
    //+/////////////////
    
    //+//////////////////////////////////////////
    // run test for small size
    //+//////////////////////////////////////////
    
    {
        const auto n = std::size_t{45};
        if (world_rank == 0)
        {
            const auto b = compute_block_size(n, MPI_COMM_WORLD);
            std::cout << "Starting the small-size test (message size ~"
                << static_cast<std::size_t>(b * b * b * sizeof(complex<double>) * 1E-6) << " MB)." << std::endl;
        }
        run_test(n, 1E-10);
        if (world_rank == 0)
        {
            std::cout << "Passed the small-size test.\n" << std::endl;
        }
    }
    
    //+//////////////////////////////////////////
    // run test for large size
    //+//////////////////////////////////////////
    
    {
        const auto n = std::size_t{360};
        if (world_rank == 0)
        {
            const auto b = compute_block_size(n, MPI_COMM_WORLD);
            std::cout << "Starting the large-size test (message size ~"
                << static_cast<std::size_t>(b * b * b * sizeof(complex<double>) * 1E-6) << " MB)" << std::endl;
        }
        run_test(n, 1E-8);
        if (world_rank == 0)
        {
            std::cout << "Passed the large-size test.\n" << std::endl;
        }
    }
    
    //+/////////////////
    } catch(const std::exception& exc)
    {
        std::cerr << "An exception was caught on process " << world_rank << "; what: " << exc.what() << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    //+/////////////////
    
    //+//////////////////////////////////////////
    // report success!
    //+//////////////////////////////////////////
    
    if (world_rank == 0)
    {
        std::cout << "All API tests have been passed!" << std::endl;
    }
    
    MPI_Finalize();
    return 0;
}

// END OF TESTFILE
