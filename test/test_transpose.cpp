//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'transpose.h'.

#include "implementation/transpose.h"
#include "implementation/schedule_thread_work.h"

#include "utility/complex_op.h"
#include "utility/core_count.h"

#include "gtest/gtest.h"

#include <omp.h>

#include <array> // std::array


using namespace s3dft;
using namespace s3dft::test;

//+//////////////////////////////////////////////
// helper function(s)
//+//////////////////////////////////////////////

template<class Treal_t>
void step_fill(tensor<Treal_t>& tens, const std::size_t& start_step)
{
    auto value = start_step;
    for (auto i1 = std::size_t{}; i1 < tens.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < tens.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < tens.get_n(); ++i3, ++value)
            {
                tens(i1, i2, i3) = {static_cast<Treal_t>(value), static_cast<Treal_t>(value)};
            }
        }
    }
}

// macro function which compares two tensors
#define test_compare_tensor(t1, t2) \
{ \
    for (auto i1 = std::size_t{}; i1 < t1.get_n(); ++i1) \
    { \
        for (auto s_index = std::size_t{}; s_index < t1.get_slice_size(); ++s_index) \
        { \
            ASSERT_NEAR(t1.get_slice_data(i1)[s_index].real, t2.get_slice_data(i1)[s_index].real, 1E-15); \
            ASSERT_NEAR(t1.get_slice_data(i1)[s_index].imag, t2.get_slice_data(i1)[s_index].imag, 1E-15); \
        } \
    } \
}



//+//////////////////////////////////////////////
// transpose in 1-3 plane
//+//////////////////////////////////////////////

template<class Treal_t>
void test_transpose13_twice_small()
{
    // setup the domain
    auto tens = tensor<Treal_t>(3);
    step_fill(tens, 1);
    
    // perform and test the transposition
    auto result = tensor<Treal_t>(3);
    #pragma omp parallel num_threads(g_core_count)
    {
        transpose13(tens, result);
    }
    
    ASSERT_TRUE(equals(result(0, 0, 0), {static_cast<Treal_t>(1.0), static_cast<Treal_t>(1.0)}));
    ASSERT_TRUE(equals(result(0, 0, 1), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    ASSERT_TRUE(equals(result(0, 0, 2), {static_cast<Treal_t>(19.0), static_cast<Treal_t>(19.0)}));
    ASSERT_TRUE(equals(result(0, 1, 0), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(result(0, 1, 1), {static_cast<Treal_t>(13.0), static_cast<Treal_t>(13.0)}));
    ASSERT_TRUE(equals(result(0, 1, 2), {static_cast<Treal_t>(22.0), static_cast<Treal_t>(22.0)}));
    ASSERT_TRUE(equals(result(0, 2, 0), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(result(0, 2, 1), {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(result(0, 2, 2), {static_cast<Treal_t>(25.0), static_cast<Treal_t>(25.0)}));
    
    ASSERT_TRUE(equals(result(1, 0, 0), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(result(1, 0, 1), {static_cast<Treal_t>(11.0), static_cast<Treal_t>(11.0)}));
    ASSERT_TRUE(equals(result(1, 0, 2), {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    ASSERT_TRUE(equals(result(1, 1, 0), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(result(1, 1, 1), {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(result(1, 1, 2), {static_cast<Treal_t>(23.0), static_cast<Treal_t>(23.0)}));
    ASSERT_TRUE(equals(result(1, 2, 0), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(result(1, 2, 1), {static_cast<Treal_t>(17.0), static_cast<Treal_t>(17.0)}));
    ASSERT_TRUE(equals(result(1, 2, 2), {static_cast<Treal_t>(26.0), static_cast<Treal_t>(26.0)}));
    
    ASSERT_TRUE(equals(result(2, 0, 0), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(result(2, 0, 1), {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(result(2, 0, 2), {static_cast<Treal_t>(21.0), static_cast<Treal_t>(21.0)}));
    ASSERT_TRUE(equals(result(2, 1, 0), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(result(2, 1, 1), {static_cast<Treal_t>(15.0), static_cast<Treal_t>(15.0)}));
    ASSERT_TRUE(equals(result(2, 1, 2), {static_cast<Treal_t>(24.0), static_cast<Treal_t>(24.0)}));
    ASSERT_TRUE(equals(result(2, 2, 0), {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    ASSERT_TRUE(equals(result(2, 2, 1), {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    ASSERT_TRUE(equals(result(2, 2, 2), {static_cast<Treal_t>(27.0), static_cast<Treal_t>(27.0)}));
    
    // perform the transposition once again
    #pragma omp parallel num_threads(g_core_count)
    {
        transpose13(result, tens);
    }
    ASSERT_TRUE(equals(tens(0, 0, 0), {static_cast<Treal_t>(1.0), static_cast<Treal_t>(1.0)}));
    ASSERT_TRUE(equals(tens(0, 0, 1), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(tens(0, 0, 2), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(tens(0, 1, 0), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(tens(0, 1, 1), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(tens(0, 1, 2), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(tens(0, 2, 0), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(tens(0, 2, 1), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(tens(0, 2, 2), {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    
    ASSERT_TRUE(equals(tens(1, 0, 0), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    ASSERT_TRUE(equals(tens(1, 0, 1), {static_cast<Treal_t>(11.0), static_cast<Treal_t>(11.0)}));
    ASSERT_TRUE(equals(tens(1, 0, 2), {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(tens(1, 1, 0), {static_cast<Treal_t>(13.0), static_cast<Treal_t>(13.0)}));
    ASSERT_TRUE(equals(tens(1, 1, 1), {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(tens(1, 1, 2), {static_cast<Treal_t>(15.0), static_cast<Treal_t>(15.0)}));
    ASSERT_TRUE(equals(tens(1, 2, 0), {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(tens(1, 2, 1), {static_cast<Treal_t>(17.0), static_cast<Treal_t>(17.0)}));
    ASSERT_TRUE(equals(tens(1, 2, 2), {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    
    ASSERT_TRUE(equals(tens(2, 0, 0), {static_cast<Treal_t>(19.0), static_cast<Treal_t>(19.0)}));
    ASSERT_TRUE(equals(tens(2, 0, 1), {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    ASSERT_TRUE(equals(tens(2, 0, 2), {static_cast<Treal_t>(21.0), static_cast<Treal_t>(21.0)}));
    ASSERT_TRUE(equals(tens(2, 1, 0), {static_cast<Treal_t>(22.0), static_cast<Treal_t>(22.0)}));
    ASSERT_TRUE(equals(tens(2, 1, 1), {static_cast<Treal_t>(23.0), static_cast<Treal_t>(23.0)}));
    ASSERT_TRUE(equals(tens(2, 1, 2), {static_cast<Treal_t>(24.0), static_cast<Treal_t>(24.0)}));
    ASSERT_TRUE(equals(tens(2, 2, 0), {static_cast<Treal_t>(25.0), static_cast<Treal_t>(25.0)}));
    ASSERT_TRUE(equals(tens(2, 2, 1), {static_cast<Treal_t>(26.0), static_cast<Treal_t>(26.0)}));
    ASSERT_TRUE(equals(tens(2, 2, 2), {static_cast<Treal_t>(27.0), static_cast<Treal_t>(27.0)}));
}

TEST(testsuite_transpose13, transpose13_twice_small_sp)
{
    test_transpose13_twice_small<float>();
}

TEST(testsuite_transpose13, transpose13_twice_small_dp)
{
    test_transpose13_twice_small<double>();
}

template<class Treal_t>
void test_transpose13_twice_large()
{
    // setup the domain
    auto tens = tensor<Treal_t>(47);
    step_fill(tens, 1);
    const auto tens_reference = tens;
    
    // first transpose
    auto result = tensor<Treal_t>(47);
    #pragma omp parallel num_threads(g_core_count)
    {
        transpose13(tens, result);
    }
    
    // compare using naive transpose as solution function
    auto tens_tr_solution = tensor<Treal_t>(tens_reference.get_n());
    for (auto i1 = std::size_t{}; i1 < tens_reference.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < tens_reference.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < tens_reference.get_n(); ++i3)
            {
                tens_tr_solution(i3, i2, i1) = tens_reference(i1, i2, i3);
            }
        }
    }
    test_compare_tensor(tens_tr_solution, result);
    
    // second transpose
    #pragma omp parallel num_threads(g_core_count)
    {
        transpose13(result, tens);
    }
    test_compare_tensor(tens_reference, tens);
}


TEST(testsuite_transpose13, transpose13_twice_large_sp)
{
    test_transpose13_twice_large<float>();
}

TEST(testsuite_transpose13, transpose13_twice_large_dp)
{
    test_transpose13_twice_large<double>();
}




//+//////////////////////////////////////////////
// transpose in 2-3 plane
//+//////////////////////////////////////////////

template<class Treal_t>
void test_transpose23_twice_small()
{
    // setup the domain
    auto tens = tensor<Treal_t>(3);
    step_fill(tens, 1);
    
    // perform and test the transposition
    auto result = tensor<Treal_t>(3);
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(tens.get_n(), 1);
        transpose23(tens, result, work_indices);
    }
    
    ASSERT_TRUE(equals(result(0, 0, 0), {static_cast<Treal_t>(1.0), static_cast<Treal_t>(1.0)}));
    ASSERT_TRUE(equals(result(0, 0, 1), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(result(0, 0, 2), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(result(0, 1, 0), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(result(0, 1, 1), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(result(0, 1, 2), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(result(0, 2, 0), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(result(0, 2, 1), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(result(0, 2, 2), {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    
    ASSERT_TRUE(equals(result(1, 0, 0), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    ASSERT_TRUE(equals(result(1, 0, 1), {static_cast<Treal_t>(13.0), static_cast<Treal_t>(13.0)}));
    ASSERT_TRUE(equals(result(1, 0, 2), {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(result(1, 1, 0), {static_cast<Treal_t>(11.0), static_cast<Treal_t>(11.0)}));
    ASSERT_TRUE(equals(result(1, 1, 1), {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(result(1, 1, 2), {static_cast<Treal_t>(17.0), static_cast<Treal_t>(17.0)}));
    ASSERT_TRUE(equals(result(1, 2, 0), {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(result(1, 2, 1), {static_cast<Treal_t>(15.0), static_cast<Treal_t>(15.0)}));
    ASSERT_TRUE(equals(result(1, 2, 2), {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    
    ASSERT_TRUE(equals(result(2, 0, 0), {static_cast<Treal_t>(19.0), static_cast<Treal_t>(19.0)}));
    ASSERT_TRUE(equals(result(2, 0, 1), {static_cast<Treal_t>(22.0), static_cast<Treal_t>(22.0)}));
    ASSERT_TRUE(equals(result(2, 0, 2), {static_cast<Treal_t>(25.0), static_cast<Treal_t>(25.0)}));
    ASSERT_TRUE(equals(result(2, 1, 0), {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    ASSERT_TRUE(equals(result(2, 1, 1), {static_cast<Treal_t>(23.0), static_cast<Treal_t>(23.0)}));
    ASSERT_TRUE(equals(result(2, 1, 2), {static_cast<Treal_t>(26.0), static_cast<Treal_t>(26.0)}));
    ASSERT_TRUE(equals(result(2, 2, 0), {static_cast<Treal_t>(21.0), static_cast<Treal_t>(21.0)}));
    ASSERT_TRUE(equals(result(2, 2, 1), {static_cast<Treal_t>(24.0), static_cast<Treal_t>(24.0)}));
    ASSERT_TRUE(equals(result(2, 2, 2), {static_cast<Treal_t>(27.0), static_cast<Treal_t>(27.0)}));
    
    // perform the transposition once again
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(result.get_n(), 1);
        transpose23(result, tens, work_indices);
    }
    ASSERT_TRUE(equals(tens(0, 0, 0), {static_cast<Treal_t>(1.0), static_cast<Treal_t>(1.0)}));
    ASSERT_TRUE(equals(tens(0, 0, 1), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(tens(0, 0, 2), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(tens(0, 1, 0), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(tens(0, 1, 1), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(tens(0, 1, 2), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(tens(0, 2, 0), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(tens(0, 2, 1), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(tens(0, 2, 2), {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    
    ASSERT_TRUE(equals(tens(1, 0, 0), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    ASSERT_TRUE(equals(tens(1, 0, 1), {static_cast<Treal_t>(11.0), static_cast<Treal_t>(11.0)}));
    ASSERT_TRUE(equals(tens(1, 0, 2), {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(tens(1, 1, 0), {static_cast<Treal_t>(13.0), static_cast<Treal_t>(13.0)}));
    ASSERT_TRUE(equals(tens(1, 1, 1), {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(tens(1, 1, 2), {static_cast<Treal_t>(15.0), static_cast<Treal_t>(15.0)}));
    ASSERT_TRUE(equals(tens(1, 2, 0), {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(tens(1, 2, 1), {static_cast<Treal_t>(17.0), static_cast<Treal_t>(17.0)}));
    ASSERT_TRUE(equals(tens(1, 2, 2), {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    
    ASSERT_TRUE(equals(tens(2, 0, 0), {static_cast<Treal_t>(19.0), static_cast<Treal_t>(19.0)}));
    ASSERT_TRUE(equals(tens(2, 0, 1), {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    ASSERT_TRUE(equals(tens(2, 0, 2), {static_cast<Treal_t>(21.0), static_cast<Treal_t>(21.0)}));
    ASSERT_TRUE(equals(tens(2, 1, 0), {static_cast<Treal_t>(22.0), static_cast<Treal_t>(22.0)}));
    ASSERT_TRUE(equals(tens(2, 1, 1), {static_cast<Treal_t>(23.0), static_cast<Treal_t>(23.0)}));
    ASSERT_TRUE(equals(tens(2, 1, 2), {static_cast<Treal_t>(24.0), static_cast<Treal_t>(24.0)}));
    ASSERT_TRUE(equals(tens(2, 2, 0), {static_cast<Treal_t>(25.0), static_cast<Treal_t>(25.0)}));
    ASSERT_TRUE(equals(tens(2, 2, 1), {static_cast<Treal_t>(26.0), static_cast<Treal_t>(26.0)}));
    ASSERT_TRUE(equals(tens(2, 2, 2), {static_cast<Treal_t>(27.0), static_cast<Treal_t>(27.0)}));
}

TEST(testsuite_transpose23, transpose23_twice_small_sp)
{
    test_transpose23_twice_small<float>();
}

TEST(testsuite_transpose23, transpose23_twice_small_dp)
{
    test_transpose23_twice_small<double>();
}

template<class Treal_t>
void test_transpose23_twice_large()
{
    // setup the domain
    auto tens = tensor<Treal_t>(47);
    step_fill(tens, 1);
    const auto tens_reference = tens;
    
    // first transpose
    auto result = tensor<Treal_t>(47);
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(tens.get_n(), 1);
        transpose23(tens, result, work_indices);
    }
    
    // compare using naive transpose as solution function
    auto tens_tr_solution = tensor<Treal_t>(tens_reference.get_n());
    for (auto i1 = std::size_t{}; i1 < tens_reference.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < tens_reference.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < tens_reference.get_n(); ++i3)
            {
                tens_tr_solution(i1, i3, i2) = tens_reference(i1, i2, i3);
            }
        }
    }
    test_compare_tensor(tens_tr_solution, result);
    
    // second transpose
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(result.get_n(), 1);
        transpose23(result, tens, work_indices);
    }
    test_compare_tensor(tens_reference, tens);
}


TEST(testsuite_transpose23, transpose23_twice_large_sp)
{
    test_transpose23_twice_large<float>();
}

TEST(testsuite_transpose23, transpose23_twice_large_dp)
{
    test_transpose23_twice_large<double>();
}
