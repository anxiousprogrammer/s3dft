//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains tests for 'tensor'.

#include "implementation/schedule_thread_work.h"
#include "tensor.h"

#include "utility/complex_op.h"
#include "utility/core_count.h"

#include "gtest/gtest.h"

#include <omp.h>


using namespace s3dft;
using namespace s3dft::test;

template<class Treal_t>
void test_lifecycle1()
{
    // test the constructibility
    auto test_tensor1 = tensor<Treal_t>(2);
    
    // fail cases
    #ifndef NDEBUG
    ASSERT_THROW(tensor<Treal_t>(0), std::invalid_argument);
    #endif
}

TEST(testsuite_tensor, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_tensor, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access_operator()
{
    // create a test instance
    auto test_tensor = tensor<Treal_t>(3);
    ASSERT_EQ(test_tensor.get_n(), 3);
    ASSERT_EQ(test_tensor.get_slice_size(), 9);
    ASSERT_EQ(test_tensor.size(), 27);
    
    // access using non-const-qualified variant
    auto value = std::size_t{2};
    for (auto i1 = std::size_t{}; i1 < test_tensor.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < test_tensor.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < test_tensor.get_n(); ++i3, ++value)
            {
                test_tensor(i1, i2, i3) = {static_cast<Treal_t>(value), static_cast<Treal_t>(value)};
            }
        }
    }
    
    // access using const-qualified variant
    ASSERT_TRUE(equals(test_tensor(0, 0, 0), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 0, 1), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 0, 2), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 1, 0), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 1, 1), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 1, 2), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 2, 0), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 2, 1), {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    ASSERT_TRUE(equals(test_tensor(0, 2, 2), {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    
    ASSERT_TRUE(equals(test_tensor(1, 0, 0), {static_cast<Treal_t>(11.0), static_cast<Treal_t>(11.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 0, 1), {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 0, 2), {static_cast<Treal_t>(13.0), static_cast<Treal_t>(13.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 1, 0), {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 1, 1), {static_cast<Treal_t>(15.0), static_cast<Treal_t>(15.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 1, 2), {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 2, 0), {static_cast<Treal_t>(17.0), static_cast<Treal_t>(17.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 2, 1), {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    ASSERT_TRUE(equals(test_tensor(1, 2, 2), {static_cast<Treal_t>(19.0), static_cast<Treal_t>(19.0)}));
    
    ASSERT_TRUE(equals(test_tensor(2, 0, 0), {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 0, 1), {static_cast<Treal_t>(21.0), static_cast<Treal_t>(21.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 0, 2), {static_cast<Treal_t>(22.0), static_cast<Treal_t>(22.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 1, 0), {static_cast<Treal_t>(23.0), static_cast<Treal_t>(23.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 1, 1), {static_cast<Treal_t>(24.0), static_cast<Treal_t>(24.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 1, 2), {static_cast<Treal_t>(25.0), static_cast<Treal_t>(25.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 2, 0), {static_cast<Treal_t>(26.0), static_cast<Treal_t>(26.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 2, 1), {static_cast<Treal_t>(27.0), static_cast<Treal_t>(27.0)}));
    ASSERT_TRUE(equals(test_tensor(2, 2, 2), {static_cast<Treal_t>(28.0), static_cast<Treal_t>(28.0)}));
    
    #ifndef NDEBUG
    ASSERT_THROW(test_tensor(0, 0, 3), std::invalid_argument);
    ASSERT_THROW(test_tensor(0, 3, 0), std::invalid_argument);
    ASSERT_THROW(test_tensor(3, 0, 0), std::invalid_argument);
    #endif
}

TEST(testsuite_tensor, access_operator_sp)
{
    test_access_operator<float>();
}

TEST(testsuite_tensor, access_operator_dp)
{
    test_access_operator<double>();
}


template<class Treal_t>
void test_access_get_slice_data()
{
    // create a test instance
    auto test_tensor = tensor<Treal_t>(3);
    
    // access using non-const-qualified variant
    ASSERT_EQ(test_tensor.get_n(), 3);
    ASSERT_EQ(test_tensor.get_slice_size(), 9);
    ASSERT_EQ(test_tensor.size(), 27);
    auto value = std::size_t{2};
    for (auto i1 = std::size_t{}; i1 < test_tensor.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < test_tensor.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < test_tensor.get_n(); ++i3, ++value)
            {
                test_tensor(i1, i2, i3) = {static_cast<Treal_t>(value), static_cast<Treal_t>(value)};
            }
        }
    }
    
    // access using const-qualified variant
    auto* slice_data = test_tensor.get_slice_data(0);
    ASSERT_TRUE(equals(slice_data[0], {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
    ASSERT_TRUE(equals(slice_data[1], {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
    ASSERT_TRUE(equals(slice_data[2], {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
    ASSERT_TRUE(equals(slice_data[3], {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
    ASSERT_TRUE(equals(slice_data[4], {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
    ASSERT_TRUE(equals(slice_data[5], {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
    ASSERT_TRUE(equals(slice_data[6], {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    ASSERT_TRUE(equals(slice_data[7], {static_cast<Treal_t>(9.0), static_cast<Treal_t>(9.0)}));
    ASSERT_TRUE(equals(slice_data[8], {static_cast<Treal_t>(10.0), static_cast<Treal_t>(10.0)}));
    
    slice_data = test_tensor.get_slice_data(1);
    ASSERT_TRUE(equals(slice_data[0], {static_cast<Treal_t>(11.0), static_cast<Treal_t>(11.0)}));
    ASSERT_TRUE(equals(slice_data[1], {static_cast<Treal_t>(12.0), static_cast<Treal_t>(12.0)}));
    ASSERT_TRUE(equals(slice_data[2], {static_cast<Treal_t>(13.0), static_cast<Treal_t>(13.0)}));
    ASSERT_TRUE(equals(slice_data[3], {static_cast<Treal_t>(14.0), static_cast<Treal_t>(14.0)}));
    ASSERT_TRUE(equals(slice_data[4], {static_cast<Treal_t>(15.0), static_cast<Treal_t>(15.0)}));
    ASSERT_TRUE(equals(slice_data[5], {static_cast<Treal_t>(16.0), static_cast<Treal_t>(16.0)}));
    ASSERT_TRUE(equals(slice_data[6], {static_cast<Treal_t>(17.0), static_cast<Treal_t>(17.0)}));
    ASSERT_TRUE(equals(slice_data[7], {static_cast<Treal_t>(18.0), static_cast<Treal_t>(18.0)}));
    ASSERT_TRUE(equals(slice_data[8], {static_cast<Treal_t>(19.0), static_cast<Treal_t>(19.0)}));
    
    slice_data = test_tensor.get_slice_data(2);
    ASSERT_TRUE(equals(slice_data[0], {static_cast<Treal_t>(20.0), static_cast<Treal_t>(20.0)}));
    ASSERT_TRUE(equals(slice_data[1], {static_cast<Treal_t>(21.0), static_cast<Treal_t>(21.0)}));
    ASSERT_TRUE(equals(slice_data[2], {static_cast<Treal_t>(22.0), static_cast<Treal_t>(22.0)}));
    ASSERT_TRUE(equals(slice_data[3], {static_cast<Treal_t>(23.0), static_cast<Treal_t>(23.0)}));
    ASSERT_TRUE(equals(slice_data[4], {static_cast<Treal_t>(24.0), static_cast<Treal_t>(24.0)}));
    ASSERT_TRUE(equals(slice_data[5], {static_cast<Treal_t>(25.0), static_cast<Treal_t>(25.0)}));
    ASSERT_TRUE(equals(slice_data[6], {static_cast<Treal_t>(26.0), static_cast<Treal_t>(26.0)}));
    ASSERT_TRUE(equals(slice_data[7], {static_cast<Treal_t>(27.0), static_cast<Treal_t>(27.0)}));
    ASSERT_TRUE(equals(slice_data[8], {static_cast<Treal_t>(28.0), static_cast<Treal_t>(28.0)}));
}

TEST(testsuite_tensor, access_data_sp)
{
    test_access_get_slice_data<float>();
}

TEST(testsuite_tensor, access_data_dp)
{
    test_access_get_slice_data<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    auto test_tensor = tensor<Treal_t>(2);
    ASSERT_EQ(test_tensor.get_n(), 2);
    ASSERT_EQ(test_tensor.get_slice_size(), 4);
    ASSERT_EQ(test_tensor.size(), 8);
    
    // fill up the test instance
    auto value = std::size_t{1};
    for (auto i1 = std::size_t{}; i1 < test_tensor.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < test_tensor.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < test_tensor.get_n(); ++i3, ++value)
            {
                test_tensor(i1, i2, i3) = {static_cast<Treal_t>(value), static_cast<Treal_t>(value)};
            }
        }
    }
    
    // test lambda
    const auto test_contents = [&test_tensor](const auto& vector)
    {
        ASSERT_EQ(test_tensor.get_n(), 2);
        ASSERT_EQ(test_tensor.get_slice_size(), 4);
        ASSERT_EQ(test_tensor.size(), 8);
        
        ASSERT_TRUE(equals(test_tensor(0, 0, 0), {static_cast<Treal_t>(1.0), static_cast<Treal_t>(1.0)}));
        ASSERT_TRUE(equals(test_tensor(0, 0, 1), {static_cast<Treal_t>(2.0), static_cast<Treal_t>(2.0)}));
        ASSERT_TRUE(equals(test_tensor(0, 1, 0), {static_cast<Treal_t>(3.0), static_cast<Treal_t>(3.0)}));
        ASSERT_TRUE(equals(test_tensor(0, 1, 1), {static_cast<Treal_t>(4.0), static_cast<Treal_t>(4.0)}));
        ASSERT_TRUE(equals(test_tensor(1, 0, 0), {static_cast<Treal_t>(5.0), static_cast<Treal_t>(5.0)}));
        ASSERT_TRUE(equals(test_tensor(1, 0, 1), {static_cast<Treal_t>(6.0), static_cast<Treal_t>(6.0)}));
        ASSERT_TRUE(equals(test_tensor(1, 1, 0), {static_cast<Treal_t>(7.0), static_cast<Treal_t>(7.0)}));
        ASSERT_TRUE(equals(test_tensor(1, 1, 1), {static_cast<Treal_t>(8.0), static_cast<Treal_t>(8.0)}));
    };
    
    // copy constructor
    auto test_tensor2 = test_tensor;
    test_contents(test_tensor2);
    
    // move constructor
    const auto test_tensor3 = tensor(std::move(test_tensor2)); // forced move constructor
    test_contents(test_tensor3);
    
    // copy operator
    test_tensor2 = test_tensor3; // forced copy operator
    test_contents(test_tensor2);
    test_contents(test_tensor3);
    
    // move operator
    const auto test_tensor4 = std::move(test_tensor2); // forced move operator
    test_contents(test_tensor4);
    ASSERT_EQ(test_tensor2.get_n(), 0);
    ASSERT_EQ(test_tensor2.get_slice_size(), 0);
    ASSERT_EQ(test_tensor2.size(), 0);
    #ifndef NDEBUG
    ASSERT_THROW(test_tensor2(0, 0, 0), std::invalid_argument);
    #endif
}

TEST(testsuite_tensor, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_tensor, lifecycle2_dp)
{
    test_lifecycle2<double>();
}


template<class Treal_t>
void test_zero_copy_functions()
{
    // lambda to fill a tensor
    const auto fill_tensor = [](tensor<Treal_t>& tens)
    {
        auto value = std::size_t{1};
        for (auto i1 = std::size_t{}; i1 < tens.get_n(); ++i1)
        {
            for (auto i2 = std::size_t{}; i2 < tens.get_n(); ++i2)
            {
                for (auto i3 = std::size_t{}; i3 < tens.get_n(); ++i3, ++value)
                {
                    tens(i1, i2, i3) = {static_cast<Treal_t>(value), static_cast<Treal_t>(value)};
                }
            }
        }
    };
    
    // create a test instance and fill it with some distinct values
    auto test_tensor = tensor<Treal_t>(3);
    fill_tensor(test_tensor);
    
    // test the zero function
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(test_tensor.get_n(), 1);
        tensor_zero(test_tensor, work_indices);
    }
    for (auto i1 = std::size_t{}; i1 < test_tensor.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < test_tensor.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < test_tensor.get_n(); ++i3)
            {
                ASSERT_TRUE(equals(test_tensor(i1, i2, i3), {static_cast<Treal_t>(0.0), static_cast<Treal_t>(0.0)}));
            }
        }
    }
    
    // test the copy function
    fill_tensor(test_tensor);
    auto test_tensor_copy = tensor<Treal_t>(3);
    #pragma omp parallel num_threads(g_core_count)
    {
        const auto work_indices = schedule_thread_work(test_tensor.get_n(), 1);
        tensor_copy(test_tensor, test_tensor_copy, work_indices);
    }
    for (auto i1 = std::size_t{}; i1 < test_tensor.get_n(); ++i1)
    {
        for (auto i2 = std::size_t{}; i2 < test_tensor.get_n(); ++i2)
        {
            for (auto i3 = std::size_t{}; i3 < test_tensor.get_n(); ++i3)
            {
                ASSERT_TRUE(equals(test_tensor(i1, i2, i3), test_tensor_copy(i1, i2, i3)));
            }
        }
    }
}

TEST(testsuite_tensor, zero_copy_functions_sp)
{
    test_zero_copy_functions<float>();
}

TEST(testsuite_tensor, zero_copy_functions_dp)
{
    test_zero_copy_functions<double>();
}

// END OF TESTFILE
