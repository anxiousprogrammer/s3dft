//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'distr_mem'.

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "implementation/tensor_matrix_math.h"
#include "matrix.h"
#include "tensor.h"
#include "distr_mem_3d_dft.h"

#include "mkl.h"

#include <omp.h>

#include <cmath> // std::atan, std::cos, std::sin
#include <algorithm> // std::copy
#include <stdexcept> // std::runtime_error


namespace s3dft
{

//+//////////////////////////////////////////////
// Implementation
//+//////////////////////////////////////////////

namespace implementation
{

template<class Treal_t>
matrix<Treal_t> compute_coeff_matrix(const std::size_t& n, const std::size_t& b, const std::size_t& n1, const std::size_t& k1)
{
    // pre-conditions
    #ifndef NDEBUG
    if (b < 1)
    {
        throw std::invalid_argument("compute_coeff_matrix_invalid_block_size_error");
    }
    if (n < b || n < n1 || n < k1)
    {
        throw std::invalid_argument("compute_coeff_matrix_invalid_sizes_error");
    }
    #endif

    // constants
    const auto pi_t_2 = std::atan(static_cast<Treal_t>(1.0)) * static_cast<Treal_t>(8.0);
    const auto n_inv = static_cast<Treal_t>(1.0) / static_cast<Treal_t>(n);
    const auto k = pi_t_2 * n_inv;
    
    // calculate the coefficients
    auto result = matrix<Treal_t>(b);
    #pragma omp parallel for
    for (auto n_index = std::size_t(0); n_index < b; ++n_index)
    {
        for (auto k_index = std::size_t{}; k_index < b; ++k_index)
        {
            const auto param = k * static_cast<Treal_t>(n1 + n_index) * static_cast<Treal_t>(k1 + k_index);
            result(n_index, k_index) = {static_cast<Treal_t>(std::cos(param)), -static_cast<Treal_t>(std::sin(param))};
        }
    }

    
    return result;
}

} // namespace implementation


std::size_t compute_block_size(const std::size_t& n, MPI_Comm comm)
{
    // pre-conditions
    if (n < 1)
    {
        throw std::invalid_argument("compute_block_size_invalid_n_error");
    }
    
    // calculate p
    auto world_size = int{};
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    const auto p_as_fp = std::cbrt(static_cast<double>(world_size));
    const auto p = static_cast<std::size_t>(p_as_fp);
    if (std::fabs(static_cast<double>(p) - p_as_fp) > 1E-15)
    {
        throw std::runtime_error("compute_block_size_non_cubic_world_size_error");
    }
    if (p == 0)
    {
        throw std::logic_error("compute_block_size_invalid_p_error");
    }
    if (n % p != 0)
    {
        throw std::runtime_error("compute_block_size_n_p_divisibility_error");
    }
    return n / p;
}

template<class Treal_t>
matrix<Treal_t> create_coeff_block_matrix(const std::size_t& n, const std::size_t& b, MPI_Comm comm)
{
    // pre-conditions
    if (n < 1)
    {
        throw std::invalid_argument("create_coeff_block_matrix_invalid_n_error");
    }
    if (b < 1)
    {
        throw std::invalid_argument("create_coeff_block_matrix_invalid_b_error");
    }
    if (n % b != 0)
    {
        throw std::invalid_argument("create_coeff_block_matrix_n_b_divisibility_error");
    }
    const auto p = n / b;
    
    // calculate proc. coordinates
    auto world_rank = int{};
    if (MPI_Comm_rank(comm, &world_rank) != MPI_SUCCESS)
    {
        throw std::runtime_error("create_coeff_block_matrix_mpi_comm_rank_error");
    }
    const auto [bi1, bi2, bi3] = get_proc_coords(p, world_rank);
    
    // calculate the coefficient matrix
    return implementation::compute_coeff_matrix<Treal_t>(n, b, bi2 * b, bi3 * b);
}

template<class Treal_t>
scratch_cache<Treal_t> create_scratch_cache(const std::size_t& b)
{
    // preconditions
    if (b < 1)
    {
        throw std::invalid_argument("create_scratch_cache_invalid_b_error");
    }
    
    auto scache = scratch_cache<Treal_t>{tensor<Treal_t>(b), tensor<Treal_t>(b), matrix<Treal_t>(b), matrix<Treal_t>(b)};
    #pragma omp parallel
    {
        if (omp_get_thread_num() == COMM_THREAD)
        {
            std::fill(scache.mat0.data(), scache.mat0.data() + scache.mat0.size(), complex<Treal_t>{});
            std::fill(scache.mat1.data(), scache.mat1.data() + scache.mat1.size(), complex<Treal_t>{});
        }
        else
        {
            const auto work_indices = schedule_thread_work(scache.tens0.get_n(), 1);
            tensor_zero(scache.tens0, work_indices);
            tensor_zero(scache.tens1, work_indices);
        }
    }
    
    return scache;
}

template<class Treal_t>
void execute(const std::size_t& n, tensor<Treal_t>& data_block, const matrix<Treal_t>& coeff_block,
    scratch_cache<Treal_t>& scache, MPI_Comm comm)
{
    // pre-conditions
    const auto& b = data_block.get_n();
    const auto p = n / b;
    #ifndef NDEBUG
    if (n % b != 0)
    {
        throw std::runtime_error("execute_n_b_divisibility_error");
    }
    if (data_block.get_n() != b || scache.tens0.get_n() != b || scache.tens1.get_n() != b)
    {
        throw std::runtime_error("execute_tensor_size_error");
    }
    if (coeff_block.get_n() != b || scache.mat0.get_n() != b || scache.mat1.get_n() != b)
    {
        throw std::runtime_error("execute_matrix_size_error");
    }
    #endif
    
    // renaming for convenience
    auto& coeff_copy = scache.mat0;
    auto& tens0 = data_block;
    auto& tens1 = scache.tens0;
    auto& tens2 = scache.tens1;
    auto& mat0 = scache.mat1;
    
    // obtain proc. rank
    auto proc_rank = int{-1};
    if (MPI_Comm_rank(comm, &proc_rank) != MPI_SUCCESS)
    {
        throw std::runtime_error("tensor_matrix_mult_mpi_comm_rank_error");
    }
    
    #pragma omp parallel
    {
        // scheduling
        const auto thread_num = omp_get_thread_num();
        const auto work_indices = schedule_thread_work(data_block.get_n(), 1);
        
        // stage 1:
        if (thread_num == COMM_THREAD)
        {
            std::copy(coeff_block.data(), coeff_block.data() + coeff_block.size(), coeff_copy.data());
        }
        else
        {
            tensor_zero(tens1, work_indices);
        }
        #pragma omp barrier
        distr_mem::tensor_matrix_mult(p, comm, proc_rank, tens0, coeff_copy, tens1, tens2, mat0, work_indices);
    
        // stage 2:
        if (thread_num == COMM_THREAD)
        {
            std::copy(coeff_block.data(), coeff_block.data() + coeff_block.size(), coeff_copy.data());
        }
        else
        {
            tensor_zero(tens2, work_indices);
        }
        #pragma omp barrier
        distr_mem::transp_matrix_tensor_mult(p, comm, proc_rank, coeff_copy, tens1, tens2, tens0, mat0, work_indices);
    
        // stage 3:
        if (thread_num == COMM_THREAD)
        {
            std::copy(coeff_block.data(), coeff_block.data() + coeff_block.size(), coeff_copy.data());
        }
        else
        {
            tensor_zero(tens0, work_indices);
        }
        #pragma omp barrier
        distr_mem::transp_tensor_matrix_mult(p, comm, proc_rank, tens2, coeff_copy, tens0, tens1, mat0, work_indices);
    }
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template matrix<float> create_coeff_block_matrix<float>(const std::size_t&, const std::size_t&, MPI_Comm);
template matrix<double> create_coeff_block_matrix<double>(const std::size_t&, const std::size_t&, MPI_Comm);

template scratch_cache<float> create_scratch_cache<float>(const std::size_t&);
template scratch_cache<double> create_scratch_cache<double>(const std::size_t&);

template void execute<float>(const std::size_t&, tensor<float>&, const matrix<float>&, scratch_cache<float>&, MPI_Comm);
template void execute<double>(const std::size_t&, tensor<double>&, const matrix<double>&, scratch_cache<double>&, MPI_Comm);

} // namespace s3dft

// END OF IMPLEMENTATION
