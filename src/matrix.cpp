//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'matrix'.

#include "matrix.h"

#include <algorithm> // std::copy


namespace s3dft
{

//+//////////////////////////////////////////////
// Lifecycle
//+//////////////////////////////////////////////

template<class Treal_t>
matrix<Treal_t>::matrix(const std::size_t& n)
    : data_(n * n), n_(n), size_(n_ * n_)
{
    // pre-conditions
    if (n == 0)
    {
        throw std::invalid_argument("matrix_ctor_invalid_size_error");
    }
}

template<class Treal_t>
matrix<Treal_t>::matrix(matrix&& rhs)
    : data_(std::move(rhs.data_)), n_(rhs.n_), size_(rhs.size_)
{
    rhs.n_ = std::size_t{};
    rhs.size_ = std::size_t{};
}

template<class Treal_t>
matrix<Treal_t>& matrix<Treal_t>::operator=(matrix&& rhs)
{
    data_ = std::move(rhs.data_);
    n_ = rhs.n_;
    size_ = rhs.size_;
    
    rhs.n_ = std::size_t{};
    rhs.size_ = std::size_t{};
    
    return *this;
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template class matrix<float>;
template class matrix<double>;

} // namespace s3dft

// END OF IMPLEMENTATION
