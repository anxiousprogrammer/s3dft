//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'shared_mem::tensor_matrix_math'.

#include "implementation/tensor_matrix_math.h"

#include "mkl.h"

#include <omp.h>

#include <type_traits> // std::is_same
#include <vector> // std::vector


namespace s3dft::shared_mem
{

//+//////////////////////////////////////////////
// Implementation
//+//////////////////////////////////////////////

namespace implementation
{

template<class Treal_t>
void cblas_xgemm(const complex<Treal_t>* m1, const complex<Treal_t>* m2, complex<Treal_t>* m3, const std::size_t& n) noexcept
{
    if constexpr (std::is_same<Treal_t, double>::value)
    {
        const auto alpha_beta = complex<double>{1.0, 0.0};
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, static_cast<const void*>(&alpha_beta),
            static_cast<const void*>(m1), n, static_cast<const void*>(m2), n, static_cast<const void*>(&alpha_beta),
            static_cast<void*>(m3), n);
    }
    else if constexpr (std::is_same<Treal_t, float>::value)
    {
        const auto alpha_beta = complex<float>{1.f, 0.f};
        cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, static_cast<const void*>(&alpha_beta),
            static_cast<const void*>(m1), n, static_cast<const void*>(m2), n, static_cast<const void*>(&alpha_beta),
            static_cast<void*>(m3), n);
    }
}

} // namespace implementation

// tensor-matrix multiplication with slices along direction 1
template<class Treal_t>
void tensor_matrix_mult(const tensor<Treal_t>& tens, const matrix<Treal_t>& mat, tensor<Treal_t>& result,
    const std::vector<std::size_t>& work_indices)
{
    // preconditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (tens.get_n() != mat.get_n() || tens.get_n() != result.get_n())
        {
            throw std::invalid_argument("shared_mem_tensor_matrix_mult_input_size_mismatch_error");
        }
    }
    #endif
    
    for (const auto& index : work_indices)
    {
        const auto* left_mat = tens.get_slice_data(index);
        auto* result_mat = result.get_slice_data(index);
        implementation::cblas_xgemm(left_mat, mat.data(), result_mat, tens.get_n());
    }
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template void tensor_matrix_mult<float>(const tensor<float>&, const matrix<float>&, tensor<float>&,
    const std::vector<std::size_t>&);
template void tensor_matrix_mult<double>(const tensor<double>&, const matrix<double>&, tensor<double>&,
    const std::vector<std::size_t>&);

} // namespace s3dft::shared_mem

// END OF IMPLEMENTATION
