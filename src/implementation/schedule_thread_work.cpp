//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation of 'schedule_thread_work.h".

#include "implementation/schedule_thread_work.h"

#include <omp.h>

#include <stdexcept> // std::invalid_argument


namespace s3dft
{

std::vector<std::size_t> schedule_thread_work(const std::size_t& work_size, const int exclude_first_threads)
{
    const auto thread_count = omp_get_num_threads();
    #ifndef NDEBUG
    if (exclude_first_threads >= thread_count)
    {
        throw std::invalid_argument("schedule_thread_work_invalid_excluded_first_threads_error");
    }
    #endif
    
    const auto thread_id = omp_get_thread_num();
    const auto worker_thread_count = thread_count - exclude_first_threads;
    auto thread_task_indices = std::vector<std::size_t>{};
    for (auto task_index = std::size_t{}; task_index < work_size; ++task_index)
    {
        if (thread_id == task_index % worker_thread_count + exclude_first_threads)
        {
            thread_task_indices.push_back(task_index);
        }
    }
    return thread_task_indices;
}

} // namespace s3dft

// END OF IMPLEMENTATION
