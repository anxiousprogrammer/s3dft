//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation of 'get_proc_plan'.

#include "implementation/proc_plan.h"

#include <stdexcept> // std::invalid_argument


namespace s3dft
{

std::size_t get_proc_id(const std::size_t& p, const std::size_t& i1, const std::size_t& i2, const std::size_t& i3)
{
    // preconditions
    #ifndef NDEBUG
    if (p == 0)
    {
        throw std::invalid_argument("get_proc_id_invalid_p_error");
    }
    #endif
    return i1 * p * p + i2 * p + i3;
}

std::array<std::size_t, 3> get_proc_coords(const std::size_t& p, const std::size_t& proc_rank)
{
    // preconditions
    #ifndef NDEBUG
    if (p == 0)
    {
        throw std::invalid_argument("get_proc_coords_invalid_p_error");
    }
    if (proc_rank >= p * p * p)
    {
        throw std::invalid_argument("get_proc_coords_invalid_proc_rank_error");
    }
    #endif
    
    // calculate the processor coordinates
    const auto p_sq = p * p;
    const auto q = proc_rank % p_sq;
    const auto i3 = q % p;

    using diff_size_t = std::make_signed<std::size_t>::type;
    const auto i2 = (static_cast<diff_size_t>(q) - static_cast<diff_size_t>(i3)) / static_cast<diff_size_t>(p);
    const auto i1 = (static_cast<diff_size_t>(proc_rank) - static_cast<diff_size_t>(q)) / static_cast<diff_size_t>(p_sq);
    if (i2 < 0 || i1 < 0)
    {
        throw std::logic_error("get_proc_coords_negative_coordinates_error");
    }
    
    // set up the processor coordinates
    return {static_cast<std::size_t>(i1), static_cast<std::size_t>(i2), static_cast<std::size_t>(i3)};
}

} // namespace s3dft

// END OF IMPLEMENTATION
