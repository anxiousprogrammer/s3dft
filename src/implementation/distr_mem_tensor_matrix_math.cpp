//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'distr_mem::tensor_matrix_math'

#include "implementation/schedule_thread_work.h"
#include "implementation/proc_plan.h"
#include "implementation/tensor_matrix_math.h"
#include "implementation/transpose.h"

#include <mpi.h>
#include <omp.h>

#include <algorithm> // std::copy
#include <stdexcept> // std::invalid_argument, std::runtime_error
#include <cmath> // std::ceil


namespace s3dft::distr_mem
{

//+//////////////////////////////////////////////
// implementation helpers
//+//////////////////////////////////////////////

namespace impl
{

struct message_packet_details
{
    std::size_t packet_count = {};
    std::size_t packet_size = {};
    std::size_t rest_packet_size = {};
};

template<class Treal_t>
void get_message_packet_details(const std::size_t& message_size, message_packet_details& output)
{    
    // all following sizes are in multiples of complex
    static constexpr auto megabyte = 1024 * 1024 / sizeof(complex<Treal_t>);
    static constexpr auto message_packet_size = MESSAGE_PACKET_SIZE * megabyte;
    
    // decide the packet count and rest packet size
    if (message_size <= message_packet_size)
    {
        output.packet_count = 1;
        output.packet_size = 0;
        output.rest_packet_size = message_size;
    }
    else
    {
        output.packet_count = static_cast<std::size_t>(std::ceil(static_cast<double>(message_size)
            / static_cast<double>(message_packet_size)));
        output.packet_size = message_packet_size;
        output.rest_packet_size = message_size % message_packet_size;
    }
    
    #ifndef NDEBUG
    if (output.packet_count < 1)
    {
        throw std::logic_error("impl_get_message_packet_details_invalid_packet_count_error");
    }
    if (output.rest_packet_size > message_size)
    {
        throw std::logic_error("impl_get_message_packet_details_invalid_rest_packet_size_error");
    }
    #endif
}

} // namespace impl

//+//////////////////////////////////////////////
// implementation of 'tensor_matrix_mult'
//+//////////////////////////////////////////////

template<class Treal_t>
void tensor_matrix_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, tensor<Treal_t>& tens_block,
    matrix<Treal_t>& mat_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices)
{
    // pre-conditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (p < 1)
        {
            throw std::invalid_argument("tensor_matrix_mult_invalid_p_error");
        }
        if (tens_block.get_n() != matrix.get_n())
        {
            throw std::invalid_argument("tensor_matrix_mult_tensor_matrix_size_mismatch_error");
        }
        if (tens_block.size() != scratch_tens.size())
        {
            throw std::invalid_argument("tensor_matrix_mult_scratch_tensor_size_error");
        }
        if (mat_block.size() != scratch_mat.size())
        {
            throw std::invalid_argument("tensor_matrix_mult_scratch_matrix_size_error");
        }
    }
    #endif
    
    // obtain proc. coordinates
    const auto [bi1, bi2, bi3] = get_proc_coords(p, proc_rank);
    
    // thread-num
    const auto thread_num = omp_get_thread_num();
    
    // constant: MPI data-type
    static constexpr auto mpi_datatype = std::is_same<Treal_t, double>::value ? MPI_DOUBLE : MPI_FLOAT;
    
    // message-packet size
    auto mpd = impl::message_packet_details{};
    static auto requests = std::array<MPI_Request, 5000>{}; // NOTE: HARD LIMIT HERE!
    if (thread_num == COMM_THREAD)
    {
        // calculate the details
        impl::get_message_packet_details<Treal_t>(tens_block.size(), mpd);
    }
    const auto packet_count_times_2 = 2 * mpd.packet_count;
    const auto requests_size = packet_count_times_2 + 2;
    
    // re-arrange the data before starting
    if (thread_num == COMM_THREAD)
    {
        if (bi2 != 0)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, bi2, (bi3 + p + bi2) % p); // from right
            const auto dest_rank = get_proc_id(p, bi1, bi2, (bi3 + p - bi2) % p); // towards left
            for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
            {
                const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                const auto irecv_res = MPI_Irecv(scratch_tens.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    src_rank, 0, comm, &requests[2 * packet_index]);
                const auto isend_res = MPI_Isend(tens_block.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    dest_rank, 0, comm, &requests[2 * packet_index + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                }
            }
        }
        if (bi3 != 0)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, (bi2 + p + bi3) % p, bi3); // from below
            const auto dest_rank = get_proc_id(p, bi1, (bi2 + p - bi3) % p, bi3); // towards up
            const auto irecv_res = MPI_Irecv(scratch_mat.data(), 2 * scratch_mat.size(), mpi_datatype, src_rank, 1, comm,
                &requests[packet_count_times_2]);
            const auto isend_res = MPI_Isend(mat_block.data(), 2 * mat_block.size(), mpi_datatype, dest_rank, 1, comm,
                &requests[packet_count_times_2 + 1]);
            if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
            }
        }
        else
        {
            // copy the current data into the send buffer
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), scratch_mat.data());
        }
        
        // waiting...
        if (bi2 != 0 && bi3 == 0) // tensor only
        {
            if (MPI_SUCCESS != MPI_Waitall(packet_count_times_2, requests.data(), MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
            }
        }
        else if (bi2 == 0 && bi3 != 0) // matrix only
        {
            if (MPI_SUCCESS != MPI_Waitall(2, requests.data() + packet_count_times_2, MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
            }
        }
        else if (bi3 != 0 && bi2 != 0) // matrix and tensor
        {
            if (MPI_SUCCESS != MPI_Waitall(requests_size, requests.data(), MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
            }
        }
    }
    if (bi2 == 0 && thread_num > COMM_THREAD)
    {
        // copy the current data into the send buffer
        tensor_copy(tens_block, scratch_tens, work_indices);
    }
    #pragma omp barrier

    // mapping
    const auto dest_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p - 1) % p); // towards left
    const auto dest_rank_mat = get_proc_id(p, bi1, (bi2 + p - 1) % p, bi3); // towards up
    const auto src_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p + 1) % p); // from right
    const auto src_rank_mat = get_proc_id(p, bi1, (bi2 + p + 1) % p, bi3); // from below
    
    // loop
    for (auto index = std::size_t{}; index < p; ++index)
    {
        // renaming for readability
        const auto buffer_selection_condition = index % 2 != 0;
        auto& tens_input = buffer_selection_condition ? tens_block : scratch_tens;
        auto& tens_recv_buffer = buffer_selection_condition ? scratch_tens : tens_block;
        auto& mat_input = buffer_selection_condition ? mat_block : scratch_mat;
        auto& mat_recv_buffer = buffer_selection_condition ? scratch_mat : mat_block;
        
        if (thread_num == COMM_THREAD)
        {
            if (index < p-1)
            {
                for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
                {
                    const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                    const auto irecv_res = MPI_Irecv(tens_recv_buffer.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, src_rank_tens, 2, comm, &requests[2 * packet_index]);
                    const auto isend_res = MPI_Isend(tens_input.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, dest_rank_tens, 2, comm, &requests[2 * packet_index + 1]);
                    if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                    {
                        throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                    }
                }
                const auto irecv_res = MPI_Irecv(mat_recv_buffer.data(), 2 * mat_recv_buffer.size(), mpi_datatype, src_rank_mat, 3,
                    comm, &requests[packet_count_times_2]);
                const auto isend_res = MPI_Isend(mat_input.data(), 2 * mat_input.size(), mpi_datatype, dest_rank_mat, 3, comm,
                    &requests[packet_count_times_2 + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_sendrecv_error");
                }
                
                // waiting...
                if (MPI_SUCCESS != MPI_Waitall(requests_size, requests.data(), MPI_STATUSES_IGNORE))
                {
                    throw std::runtime_error("tensor_matrix_mult_mpi_waitall_error");
                }
            }
        }
        else
        {
            // compute the product
            ::s3dft::shared_mem::tensor_matrix_mult(tens_input, mat_input, result_block, work_indices);
        }
        #pragma omp barrier
    }
}


//+//////////////////////////////////////////////
// implementation of
// 'transp_matrix_tensor_mult'
//+//////////////////////////////////////////////

template<class Treal_t>
void transp_matrix_tensor_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, matrix<Treal_t>& mat_block,
    tensor<Treal_t>& tens_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices)
{
    // pre-conditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (p < 1)
        {
            throw std::invalid_argument("transp_matrix_tensor_mult_invalid_p_error");
        }
        if (tens_block.get_n() != matrix.get_n())
        {
            throw std::invalid_argument("transp_matrix_tensor_mult_tensor_matrix_size_mismatch_error");
        }
        if (tens_block.size() != scratch_tens.size())
        {
            throw std::invalid_argument("transp_matrix_tensor_mult_scratch_tensor_size_error");
        }
        if (mat_block.size() != scratch_mat.size())
        {
            throw std::invalid_argument("transp_matrix_tensor_mult_scratch_matrix_size_error");
        }
    }
    #endif
    
    // transpose the current data into the send buffer
    transpose23(tens_block, scratch_tens, work_indices);
    #pragma omp barrier
    
    // obtain proc. coordinates
    const auto [bi1, bi2, bi3] = get_proc_coords(p, proc_rank);
    
    // thread-num
    const auto thread_num = omp_get_thread_num();
    
    // constant: MPI data-type
    static constexpr auto mpi_datatype = std::is_same<Treal_t, double>::value ? MPI_DOUBLE : MPI_FLOAT;
    
    // message-packet size
    auto mpd = impl::message_packet_details{};
    static auto requests = std::array<MPI_Request, 5000>{}; // NOTE: HARD LIMIT HERE!
    if (thread_num == COMM_THREAD)
    {
        // calculate the details
        impl::get_message_packet_details<Treal_t>(tens_block.size(), mpd);
    }
    const auto packet_count_times_2 = 2 * mpd.packet_count;
    const auto requests_size = packet_count_times_2 + 2;
    
    // re-arrange the data before starting
    if (thread_num == COMM_THREAD)
    {
        // global transpose is a lesser evil compared to all those high latency hops (in a loop)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, (bi3 + p + bi2) % p, bi2); // from right (transposed in 23 plane!)
            const auto dest_rank = get_proc_id(p, bi1, bi3, (bi2 + p - bi3) % p); // towards left (transposed in 23 plane!)
            for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
            {
                const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                const auto irecv_res = MPI_Irecv(tens_block.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    src_rank, 0, comm, &requests[2 * packet_index]);
                const auto isend_res = MPI_Isend(scratch_tens.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    dest_rank, 0, comm, &requests[2 * packet_index + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("transp_matrix_tensor_mult_mpi_sendrecv_error");
                }
            }
        }
        if (bi3 != 0)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, (bi2 + p + bi3) % p, bi3); // from below
            const auto dest_rank = get_proc_id(p, bi1, (bi2 + p - bi3) % p, bi3); // towards up
            const auto irecv_res = MPI_Irecv(scratch_mat.data(), 2 * scratch_mat.size(), mpi_datatype, src_rank, 1, comm,
                &requests[packet_count_times_2]);
            const auto isend_res = MPI_Isend(mat_block.data(), 2 * mat_block.size(), mpi_datatype, dest_rank, 1, comm,
                &requests[packet_count_times_2 + 1]);
            if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
            {
                throw std::runtime_error("transp_matrix_tensor_mult_mpi_sendrecv_error");
            }
        }
        else
        {
            // copy the current data into the send buffer
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), scratch_mat.data());
        }
        
        // waiting...
        if (MPI_SUCCESS != MPI_Waitall(packet_count_times_2, requests.data(), MPI_STATUSES_IGNORE)) // tensor only
        {
            throw std::runtime_error("transp_matrix_tensor_mult_mpi_waitall_error");
        }
        if (bi3 != 0) // if matrix was moved
        {
            if (MPI_SUCCESS != MPI_Waitall(2, requests.data() + packet_count_times_2, MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("transp_matrix_tensor_mult_mpi_waitall_error");
            }
        }
    }
    #pragma omp barrier

    // mapping
    const auto dest_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p - 1) % p); // towards left
    const auto dest_rank_mat = get_proc_id(p, bi1, (bi2 + p - 1) % p, bi3); // towards up
    const auto src_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p + 1) % p); // from right
    const auto src_rank_mat = get_proc_id(p, bi1, (bi2 + p + 1) % p, bi3); // from below
    
    // loop
    for (auto index = std::size_t{}; index < p; ++index)
    {
        // renaming for readability
        const auto buffer_selection_condition = index % 2 != 0;
        auto& tens_input = !buffer_selection_condition ? tens_block : scratch_tens;
        auto& tens_recv_buffer = !buffer_selection_condition ? scratch_tens : tens_block;
        auto& mat_input = buffer_selection_condition ? mat_block : scratch_mat;
        auto& mat_recv_buffer = buffer_selection_condition ? scratch_mat : mat_block;
        
        if (thread_num == COMM_THREAD)
        {
            if (index < p-1)
            {
                for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
                {
                    const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                    const auto irecv_res = MPI_Irecv(tens_recv_buffer.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, src_rank_tens, 2, comm, &requests[2 * packet_index]);
                    const auto isend_res = MPI_Isend(tens_input.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, dest_rank_tens, 2, comm, &requests[2 * packet_index + 1]);
                    if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                    {
                        throw std::runtime_error("transp_matrix_tensor_mult_mpi_sendrecv_error");
                    }
                }
                const auto irecv_res = MPI_Irecv(mat_recv_buffer.data(), 2 * mat_recv_buffer.size(), mpi_datatype, src_rank_mat, 3,
                    comm, &requests[packet_count_times_2]);
                const auto isend_res = MPI_Isend(mat_input.data(), 2 * mat_input.size(), mpi_datatype, dest_rank_mat, 3, comm,
                    &requests[packet_count_times_2 + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("transp_matrix_tensor_mult_mpi_sendrecv_error");
                }
                
                // waiting...
                if (MPI_SUCCESS != MPI_Waitall(requests_size, requests.data(), MPI_STATUSES_IGNORE))
                {
                    throw std::runtime_error("transp_matrix_tensor_mult_mpi_waitall_error");
                }
            }
        }
        else
        {
            // compute the product
            ::s3dft::shared_mem::tensor_matrix_mult(tens_input, mat_input, result_block, work_indices);
        }
        #pragma omp barrier
    }
    
    // global transpose once again!
    if (thread_num == COMM_THREAD)
    {
        // ring messaging
        const auto src_rank = get_proc_id(p, bi1, bi3, bi2); // transpose in 23 plane
        const auto dest_rank = get_proc_id(p, bi1, bi3, bi2); // transpose in 23 plane
        for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
        {
            const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
            const auto irecv_res = MPI_Irecv(scratch_tens.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                src_rank, 0, comm, &requests[2 * packet_index]);
            const auto isend_res = MPI_Isend(result_block.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                dest_rank, 0, comm, &requests[2 * packet_index + 1]);
            if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
            {
                throw std::runtime_error("transp_matrix_tensor_mult_mpi_sendrecv_error");
            }
        }
        
        // waiting...
        if (MPI_SUCCESS != MPI_Waitall(packet_count_times_2, requests.data(), MPI_STATUSES_IGNORE))
        {
            throw std::runtime_error("transp_matrix_tensor_mult_mpi_waitall_error");
        }
    }
    #pragma omp barrier
    transpose23(scratch_tens, result_block, work_indices);
    #pragma omp barrier
}


//+//////////////////////////////////////////////
// implementation of
// 'transp_tensor_matrix_mult'
//+//////////////////////////////////////////////

template<class Treal_t>
void transp_tensor_matrix_mult(const std::size_t& p, MPI_Comm comm, const int& proc_rank, tensor<Treal_t>& tens_block,
    matrix<Treal_t>& mat_block, tensor<Treal_t>& result_block, tensor<Treal_t>& scratch_tens, matrix<Treal_t>& scratch_mat,
    const std::vector<std::size_t>& work_indices)
{
    // pre-conditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (p < 1)
        {
            throw std::invalid_argument("transp_tensor_matrix_mult_invalid_p_error");
        }
        if (tens_block.get_n() != matrix.get_n())
        {
            throw std::invalid_argument("transp_tensor_matrix_mult_tensor_matrix_size_mismatch_error");
        }
        if (tens_block.size() != scratch_tens.size())
        {
            throw std::invalid_argument("transp_tensor_matrix_mult_scratch_tensor_size_error");
        }
        if (mat_block.size() != scratch_mat.size())
        {
            throw std::invalid_argument("transp_tensor_matrix_mult_scratch_matrix_size_error");
        }
    }
    #endif
    
    // transpose the current data into the send buffer
    transpose13(tens_block, scratch_tens);
    #pragma omp barrier
    
    // obtain proc. coordinates
    const auto [bi1, bi2, bi3] = get_proc_coords(p, proc_rank);
    
    // thread-num
    const auto thread_num = omp_get_thread_num();
    
    // constant: MPI data-type
    static constexpr auto mpi_datatype = std::is_same<Treal_t, double>::value ? MPI_DOUBLE : MPI_FLOAT;
    
    // message-packet size
    auto mpd = impl::message_packet_details{};
    static auto requests = std::array<MPI_Request, 5000>{}; // NOTE: HARD LIMIT HERE!
    if (thread_num == COMM_THREAD)
    {
        // calculate the details
        impl::get_message_packet_details<Treal_t>(tens_block.size(), mpd);
    }
    const auto packet_count_times_2 = 2 * mpd.packet_count;
    const auto requests_size = packet_count_times_2 + 2;
    
    // re-arrange the data before starting
    if (thread_num == COMM_THREAD)
    {
        // global transpose is a lesser evil compared to all those high latency hops (in a loop)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, (bi3 + p + bi2) % p, bi2, bi1); // from right (transposed in 13 plane!)
            const auto dest_rank = get_proc_id(p, bi3, bi2, (bi1 + p - bi2) % p); // towards left (transposed in 13 plane!)
            for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
            {
                const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                const auto irecv_res = MPI_Irecv(tens_block.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    src_rank, 0, comm, &requests[2 * packet_index]);
                const auto isend_res = MPI_Isend(scratch_tens.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                    dest_rank, 0, comm, &requests[2 * packet_index + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("transp_tensor_matrix_mult_mpi_sendrecv_error");
                }
            }
        }
        if (bi3 != 0)
        {
            // ring messaging
            const auto src_rank = get_proc_id(p, bi1, (bi2 + p + bi3) % p, bi3); // from below
            const auto dest_rank = get_proc_id(p, bi1, (bi2 + p - bi3) % p, bi3); // towards up
            const auto irecv_res = MPI_Irecv(scratch_mat.data(), 2 * scratch_mat.size(), mpi_datatype, src_rank, 1, comm,
                &requests[packet_count_times_2]);
            const auto isend_res = MPI_Isend(mat_block.data(), 2 * mat_block.size(), mpi_datatype, dest_rank, 1, comm,
                &requests[packet_count_times_2 + 1]);
            if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
            {
                throw std::runtime_error("transp_tensor_matrix_mult_mpi_sendrecv_error");
            }
        }
        else
        {
            // copy the current data into the send buffer
            std::copy(mat_block.data(), mat_block.data() + mat_block.size(), scratch_mat.data());
        }
        
        // waiting...
        if (MPI_SUCCESS != MPI_Waitall(packet_count_times_2, requests.data(), MPI_STATUSES_IGNORE)) // tensor only
        {
            throw std::runtime_error("transp_tensor_matrix_mult_mpi_waitall_error");
        }
        if (bi3 != 0) // if matrix was moved
        {
            if (MPI_SUCCESS != MPI_Waitall(2, requests.data() + packet_count_times_2, MPI_STATUSES_IGNORE))
            {
                throw std::runtime_error("transp_tensor_matrix_mult_mpi_waitall_error");
            }
        }
    }
    #pragma omp barrier

    // mapping
    const auto dest_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p - 1) % p); // towards left
    const auto dest_rank_mat = get_proc_id(p, bi1, (bi2 + p - 1) % p, bi3); // towards up
    const auto src_rank_tens = get_proc_id(p, bi1, bi2, (bi3 + p + 1) % p); // from right
    const auto src_rank_mat = get_proc_id(p, bi1, (bi2 + p + 1) % p, bi3); // from below
    
    // loop
    for (auto index = std::size_t{}; index < p; ++index)
    {
        // renaming for readability
        const auto buffer_selection_condition = index % 2 != 0;
        auto& tens_input = !buffer_selection_condition ? tens_block : scratch_tens;
        auto& tens_recv_buffer = !buffer_selection_condition ? scratch_tens : tens_block;
        auto& mat_input = buffer_selection_condition ? mat_block : scratch_mat;
        auto& mat_recv_buffer = buffer_selection_condition ? scratch_mat : mat_block;
        
        if (thread_num == COMM_THREAD)
        {
            if (index < p-1)
            {
                for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
                {
                    const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
                    const auto irecv_res = MPI_Irecv(tens_recv_buffer.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, src_rank_tens, 2, comm, &requests[2 * packet_index]);
                    const auto isend_res = MPI_Isend(tens_input.data() + packet_index * mpd.packet_size, message_size,
                        mpi_datatype, dest_rank_tens, 2, comm, &requests[2 * packet_index + 1]);
                    if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                    {
                        throw std::runtime_error("transp_tensor_matrix_mult_mpi_sendrecv_error");
                    }
                }
                const auto irecv_res = MPI_Irecv(mat_recv_buffer.data(), 2 * mat_recv_buffer.size(), mpi_datatype, src_rank_mat, 3,
                    comm, &requests[packet_count_times_2]);
                const auto isend_res = MPI_Isend(mat_input.data(), 2 * mat_input.size(), mpi_datatype, dest_rank_mat, 3, comm,
                    &requests[packet_count_times_2 + 1]);
                if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
                {
                    throw std::runtime_error("transp_tensor_matrix_mult_mpi_sendrecv_error");
                }
                
                // waiting...
                if (MPI_SUCCESS != MPI_Waitall(requests_size, requests.data(), MPI_STATUSES_IGNORE))
                {
                    throw std::runtime_error("transp_tensor_matrix_mult_mpi_waitall_error");
                }
            }
        }
        else
        {
            // compute the product
            ::s3dft::shared_mem::tensor_matrix_mult(tens_input, mat_input, result_block, work_indices);
        }
        #pragma omp barrier
    }
    
    // global transpose once again!
    if (thread_num == COMM_THREAD)
    {
        // ring messaging
        const auto src_rank = get_proc_id(p, bi3, bi2, bi1); // transpose in 13 plane
        const auto dest_rank = get_proc_id(p, bi3, bi2, bi1); // transpose in 13 plane
        for (auto packet_index = std::size_t{}; packet_index < mpd.packet_count; ++packet_index)
        {
            const auto message_size = 2 * (packet_index < mpd.packet_count - 1 ? mpd.packet_size : mpd.rest_packet_size);
            const auto irecv_res = MPI_Irecv(scratch_tens.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                src_rank, 0, comm, &requests[2 * packet_index]);
            const auto isend_res = MPI_Isend(result_block.data() + packet_index * mpd.packet_size, message_size, mpi_datatype,
                dest_rank, 0, comm, &requests[2 * packet_index + 1]);
            if (MPI_SUCCESS != isend_res || MPI_SUCCESS != irecv_res)
            {
                throw std::runtime_error("transp_tensor_matrix_mult_mpi_sendrecv_error");
            }
        }
        
        // waiting...
        if (MPI_SUCCESS != MPI_Waitall(packet_count_times_2, requests.data(), MPI_STATUSES_IGNORE))
        {
            throw std::runtime_error("transp_tensor_matrix_mult_mpi_waitall_error");
        }
    }
    #pragma omp barrier
    transpose13(scratch_tens, result_block);
    #pragma omp barrier
}


//+//////////////////////////////////////////////
// Explicit instantiations
//+//////////////////////////////////////////////

template void tensor_matrix_mult<float>(const std::size_t&, MPI_Comm, const int&, tensor<float>&, matrix<float>&, tensor<float>&,
    tensor<float>&, matrix<float>&, const std::vector<std::size_t>&);
template void tensor_matrix_mult<double>(const std::size_t&, MPI_Comm, const int&, tensor<double>&, matrix<double>&,
    tensor<double>&, tensor<double>&, matrix<double>&, const std::vector<std::size_t>&);

template void transp_matrix_tensor_mult<float>(const std::size_t&, MPI_Comm, const int&, matrix<float>&, tensor<float>&,
    tensor<float>&, tensor<float>&, matrix<float>&, const std::vector<std::size_t>&);
template void transp_matrix_tensor_mult<double>(const std::size_t&, MPI_Comm, const int&, matrix<double>&, tensor<double>&,
    tensor<double>&, tensor<double>&, matrix<double>&, const std::vector<std::size_t>&);

template void transp_tensor_matrix_mult<float>(const std::size_t&, MPI_Comm, const int&, tensor<float>&, matrix<float>&,
    tensor<float>&, tensor<float>&, matrix<float>&, const std::vector<std::size_t>&);
template void transp_tensor_matrix_mult<double>(const std::size_t&, MPI_Comm, const int&, tensor<double>&, matrix<double>&,
    tensor<double>&, tensor<double>&, matrix<double>&, const std::vector<std::size_t>&);

} // namespace s3dft::distr_mem

// END OF IMPLEMENTATION
