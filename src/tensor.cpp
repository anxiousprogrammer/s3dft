//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation and explicit instantiations of 'tensor'.

#include "tensor.h"

#include <cmath> // std::ceil


namespace s3dft
{

//+//////////////////////////////////////////////
// Lifecycle
//+//////////////////////////////////////////////

template<class Treal_t>
tensor<Treal_t>::tensor(const std::size_t& n)
    : data_(), n_(n), n_sq_(n_ * n_), size_(n_ * n_sq_)
{
    // preconditions
    if (n == 0)
    {
        throw std::invalid_argument("tensor_ctor_invalid_size_error");
    }
    
    // TODO: leftovers: alternative slice-sizing with padding
    /*const auto page_size = sysconf(_SC_PAGESIZE) / sizeof(complex<Treal_t>);
    #ifndef NDEBUG
    if (page_size == 0)
    {
        throw std::runtime_error("tensor_ctor_invalid_page_size_error");
    }
    #endif
    slice_size_ = static_cast<std::size_t>(std::ceil(static_cast<Treal_t>(n2_ * n3_) / static_cast<Treal_t>(page_size)))
        * page_size;*/
    data_ = data_pack<Treal_t>(size_);
}

template<class Treal_t>
tensor<Treal_t>::tensor(tensor&& rhs)
    : data_(std::move(rhs.data_)), n_(rhs.n_), n_sq_(rhs.n_sq_), size_(rhs.size_)
{
    rhs.n_ = std::size_t{};
    rhs.n_sq_ = std::size_t{};
    rhs.size_ = std::size_t{};
}

template<class Treal_t>
tensor<Treal_t>& tensor<Treal_t>::operator=(tensor&& rhs)
{
    data_ = std::move(rhs.data_);
    n_ = rhs.n_;
    n_sq_ = rhs.n_sq_;
    size_ = rhs.size_;
    
    rhs.n_ = std::size_t{};
    rhs.n_sq_ = std::size_t{};
    rhs.size_ = std::size_t{};
    
    return *this;
}


//+//////////////////////////////////////////////
// Utility functions
//+//////////////////////////////////////////////

template<class Treal_t>
void tensor_zero(tensor<Treal_t>& op, const std::vector<std::size_t>& work_indices)
{
    for (const auto& index : work_indices)
    {
        auto* data_begin = op.get_slice_data(index);
        auto* data_end = data_begin + op.get_slice_size();
        std::fill(data_begin, data_end, complex<Treal_t>{});
    }
}

template<class Treal_t>
void tensor_copy(const tensor<Treal_t>& op1, tensor<Treal_t>& op2, const std::vector<std::size_t>& work_indices)
{
    // preconditions
    #ifndef NDEBUG
    #pragma omp single
    {
        if (op1.get_n() != op2.get_n())
        {
            throw std::invalid_argument("tensor_copy_operand_size_mismatch_error");
        }
    }
    #endif
    
    for (const auto& index : work_indices)
    {
        const auto* op1_begin = op1.get_slice_data(index);
        const auto* op1_end = op1_begin + op1.get_slice_size();
        auto* op2_begin = op2.get_slice_data(index);
        std::copy(op1_begin, op1_end, op2_begin);
    }
}


//+//////////////////////////////////////////////
// Explicit instantiation
//+//////////////////////////////////////////////

template class tensor<float>;
template class tensor<double>;

template void tensor_zero<float>(tensor<float>& op, const std::vector<std::size_t>&);
template void tensor_zero<double>(tensor<double>& op, const std::vector<std::size_t>&);

template void tensor_copy<float>(const tensor<float>& op1, tensor<float>& op2, const std::vector<std::size_t>&);
template void tensor_copy<double>(const tensor<double>& op1, tensor<double>& op2, const std::vector<std::size_t>&);

} // namespace s3dft

// END OF IMPLEMENTATION
